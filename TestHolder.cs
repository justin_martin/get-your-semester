﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 * Created by Justin "PheniXos" Martin
 * Ce script permet de tester des fonctionnalitées normalement non accessible a ce niveau du jeu
*/
public class TestHolder : MonoBehaviour {

	[SerializeField]
	private GameObject visite;
	[SerializeField]
	private GameObject fight;

	// Use this for initialization
	void Start () {	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.N)) {
			visite.GetComponent<VisiteManager> ().StateVisite (true); //Mode Visite
		}

		if (Input.GetKeyDown (KeyCode.F)) {
			fight.GetComponent<FightManager> ().StateFight (true); // Mode Combat
		}

		if (Input.GetKeyDown (KeyCode.G)) {
			PlayerPrefs.SetInt ("startNewGame", 1);  // Lancement parti (Mode Narrateur)
			Initiate.Fade("Game", Color.black, 2.0f);
		}

		if (Input.GetKeyDown (KeyCode.K)) {
			PlayerPrefs.DeleteAll ();
		}
	}
}
