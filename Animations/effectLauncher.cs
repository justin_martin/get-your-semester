﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class effectLauncher : MonoBehaviour {


	[SerializeField] private GameObject[] imageLoop;
	private int indexImage;
	// Use this for initialization
	void Start () {
		indexImage = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (indexImage == imageLoop.Length - 1)
			indexImage = 0;


		imageLoop [indexImage].SetActive (true);
		imageLoop [indexImage].GetComponent<FadeTarget> ().FadeIn ();
		StartCoroutine (tempoImage());
		imageLoop [indexImage].GetComponent<FadeTarget> ().FadeOut ();
		/*StartCoroutine (waitFadeOut());
		imageLoop [indexImage].SetActive (false);
		indexImage++;*/
	}

	IEnumerator waitFadeOut ()	{
		while (imageLoop [indexImage].GetComponent<FadeTarget> ().getIsIn ())
			yield return new WaitForSeconds (0.1f);

	}

	IEnumerator tempoImage (){
		yield return new WaitForSeconds (2f);
	}
}
