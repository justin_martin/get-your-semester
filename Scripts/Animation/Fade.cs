﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
 * Created By Justin "PheniXos" Martin
 * Ce script permet de faire un effet de fondu au noir avec déplacement du joueur
 **/


public class Fade : MonoBehaviour {

	[SerializeField]
	private GameObject player;

	[SerializeField]
	private GameObject Cameras;
	[SerializeField]
	private GameObject blackscreenCanvas;
	[SerializeField]
	private Image blackScreen;

	[SerializeField]
	public Color colorFade = Color.black;

	public bool fadeActive;
	// Use this for initialization
	void Start () {	}

	/**
	 * Fonctionnalité permettant de deplacer un joueur a des coordonnées X et Y avec un effet fade
	 * @return IEnumerator
	 * @params float XCoord coordonée en X ciblé
	 * @params float YCoord coordonée en Y ciblé
	 * */

	public IEnumerator DoFadeWithPlayerMove(float xCoord, float yCoord) {
		fadeActive = true;
		blackscreenCanvas.SetActive (true);
		// On défini la couleur
		blackScreen.color = colorFade;
		// On met l'opacité à 0
		blackScreen.canvasRenderer.SetAlpha (0.0f);

		// On va du transparent vers opaque
		for(float toOne = 0.0f; toOne <= 1.0f ; toOne += 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toOne);
			yield return new WaitForSeconds (0.05f);
		}

		// On déplace le joueur quand l'opacité est à fond
		player.transform.position = new Vector3 (xCoord, yCoord, -0.2f);

		// On va de l'opaque vers le transparent
		for(float toZero = 1.0f; toZero >= 0.0f ; toZero -= 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toZero);
			yield return new WaitForSeconds (0.05f);
		}

		// On désactive le Canvas
		blackscreenCanvas.SetActive (false);
		fadeActive = false;
	}

	/**
	 * Fonctionnalité permettant de deplacer un joueur a des coordonnées X et Y avec un effet fade et la cameras un d'autres coordonées
	 * @return IEnumerator
	 * @params float XCoord coordonée en X ciblé du joueur
	 * @params float YCoord coordonée en Y ciblé du joueur
	 * @params float XCoord coordonée en X ciblé de la caméras
	 * @params float YCoord coordonée en Y ciblé de la caméras
	 * */
	public IEnumerator DoFadeWithPlayerMove(float xCoord, float yCoord, float xcamCoord, float ycamCoord) {
		fadeActive = true;
		blackscreenCanvas.SetActive (true);
		// On défini la couleur
		blackScreen.color = colorFade;
		// On met l'opacité à 0
		blackScreen.canvasRenderer.SetAlpha (0.0f);

		// On va du transparent vers opaque
		for(float toOne = 0.0f; toOne <= 1.0f ; toOne += 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toOne);
			yield return new WaitForSeconds (0.05f);
		}

		// On déplace le joueur quand l'opacité est à fond
		player.transform.position = new Vector3 (xCoord, yCoord, -0.2f);
		Cameras.GetComponent<CameraFollow> ().setPositionCam (xcamCoord, ycamCoord, -10f);

		// On va de l'opaque vers le transparent
		for(float toZero = 1.0f; toZero >= 0.0f ; toZero -= 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toZero);
			yield return new WaitForSeconds (0.05f);
		}

		// On désactive le Canvas
		blackscreenCanvas.SetActive (false);
		fadeActive = false;
	}

	/**
	 * Fonctionnalité permettant de deplacer un joueur a des coordonnées X et Y avec un effet fade et de activer/desactiver le suivi de cameras
	 * @return IEnumerator
	 * @params float XCoord coordonée en X ciblé du joueur
	 * @params float YCoord coordonée en Y ciblé du joueur
	 * @params bool camActive definie si la camera suis le joueur
	 * */
	public IEnumerator DoFadeWithPlayerMove(float xCoord, float yCoord, bool camActive) {
		fadeActive = true;
		blackscreenCanvas.SetActive (true);
		// On défini la couleur
		blackScreen.color = colorFade;
		// On met l'opacité à 0
		blackScreen.canvasRenderer.SetAlpha (0.0f);

		// On va du transparent vers opaque
		for(float toOne = 0.0f; toOne <= 1.0f ; toOne += 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toOne);
			yield return new WaitForSeconds (0.05f);
		}

		// On déplace le joueur quand l'opacité est à fond
		player.transform.position = new Vector3 (xCoord, yCoord, -0.2f);
		Cameras.GetComponent<CameraFollow> ().setFollowActive(camActive);

		// On va de l'opaque vers le transparent
		for(float toZero = 1.0f; toZero >= 0.0f ; toZero -= 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toZero);
			yield return new WaitForSeconds (0.05f);
		}

		// On désactive le Canvas
		blackscreenCanvas.SetActive (false);
		fadeActive = false;
	}


	/**
	 * Fonctionnalité permettant de faire un effet fade et de activer/desactiver le suivi de cameras
	 * @return IEnumerator
	 * */
	public IEnumerator DoFadeWithoutPlayerMove() {
		fadeActive = true;
		blackscreenCanvas.SetActive (true);
		// On défini la couleur
		blackScreen.color = colorFade;
		// On met l'opacité à 0
		blackScreen.canvasRenderer.SetAlpha (0.0f);

		// On va du transparent vers opaque
		for(float toOne = 0.0f; toOne <= 1.0f ; toOne += 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toOne);
			yield return new WaitForSeconds (0.05f);
		}

		// On va de l'opaque vers le transparent
		for(float toZero = 1.0f; toZero >= 0.0f ; toZero -= 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toZero);
			yield return new WaitForSeconds (0.05f);
		}

		// On désactive le Canvas
		blackscreenCanvas.SetActive (false);
		fadeActive = false;
	}


	public IEnumerator DoFadeWithMovCam(Transform target, bool camActive) {
		fadeActive = true;
		blackscreenCanvas.SetActive (true);
		// On défini la couleur
		blackScreen.color = colorFade;
		// On met l'opacité à 0
		blackScreen.canvasRenderer.SetAlpha (0.0f);

		// On va du transparent vers opaque
		for(float toOne = 0.0f; toOne <= 1.0f ; toOne += 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toOne);
			yield return new WaitForSeconds (0.05f);
		}
		Debug.Log ("info");
		Cameras.GetComponent<CameraFollow> ().setPositionCam(target.position.x, target.position.y, -10f);

		// On va de l'opaque vers le transparent
		for(float toZero = 1.0f; toZero >= 0.0f ; toZero -= 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toZero);
			yield return new WaitForSeconds (0.05f);
		}

		// On désactive le Canvas
		blackscreenCanvas.SetActive (false);
		fadeActive = false;
	}

	public IEnumerator DoFadeFollowPlayer (bool camState) {
		fadeActive = true;
		blackscreenCanvas.SetActive (true);
		// On défini la couleur
		blackScreen.color = colorFade;
		// On met l'opacité à 0
		blackScreen.canvasRenderer.SetAlpha (0.0f);

		// On va du transparent vers opaque
		for(float toOne = 0.0f; toOne <= 1.0f ; toOne += 0.1f) {
			blackScreen.canvasRenderer.SetAlpha (toOne);
			yield return new WaitForSeconds (0.05f);
		}

		Cameras.GetComponent<CameraFollow> ().setFollowTarget (player.transform);

		// On va de l'opaque vers le transparent
		for(float toZero = 1.0f; toZero >= 0.0f ; toZero -= 0.1f)
		{
			blackScreen.canvasRenderer.SetAlpha (toZero);
			yield return new WaitForSeconds (0.05f);
		}

		// On désactive le Canvas
		blackscreenCanvas.SetActive (false);
		fadeActive = false;
	}
}
