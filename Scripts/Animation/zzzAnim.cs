﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zzzAnim : MonoBehaviour {

	public Sprite[] Sprites;
	public int framesPerSecond;

	// Use this for initialization
	void Start () {	}
	
	// Update is called once per frame
	void Update () {
		//tempo un peu pour le zz apparait quand il est afficher  dans le text
		int index = (int) (Time.time * framesPerSecond) % Sprites.Length;
		gameObject.GetComponent<SpriteRenderer> ().sprite = Sprites [index];	
	}
}
