﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class JouerMenu : MonoBehaviour {

	[SerializeField] private GameObject buttonContinue;

	void Start(){
		Debug.Log (PlayerPrefs.GetInt ("SavedGame")+"");
		if (PlayerPrefs.GetInt ("SavedGame") != 1)
			buttonContinue.GetComponent<Button> ().interactable = false;
	}

    public void StartNewGame() {
		PlayerPrefs.SetInt ("startNewGame", 1); 
        Initiate.Fade("Game", Color.black, 2.0f);
    }

	public void ResumeGame() {

		PlayerPrefs.SetInt ("resumeGame", 1); 
		Initiate.Fade("Game", Color.black, 2.0f);
	}
}
