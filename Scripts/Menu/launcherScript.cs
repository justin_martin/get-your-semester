﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class launcherScript : MonoBehaviour {
	[SerializeField]
	private GameObject fade;


	[SerializeField]
	private GameObject mainMenu;
	private bool introActive;
	// Use this for initialization
	void Start () {
		introActive = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(introActive && (Input.GetKeyDown(KeyCode.Return) || Input.touchCount>0)){
			StartCoroutine (fade.GetComponent<Fade> ().DoFadeWithoutPlayerMove());
			gameObject.SetActive (false);
			mainMenu.SetActive (true);
		}
	}
}
