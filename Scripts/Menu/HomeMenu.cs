﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeMenu : MonoBehaviour {

	public void Quitter()
    {
        // Si c'est la version Build
        #if UNITY_STANDALONE
            Application.Quit();
        #endif

        // Si c'est la version éditeur
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #endif
    }
		
}
