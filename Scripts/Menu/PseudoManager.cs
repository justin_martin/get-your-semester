﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PseudoManager : MonoBehaviour {

	public string pseudo;
	public Text inputText;

	public Sprite errorBoxBG;
	public Sprite succesBoxBG;

	public Sprite errorSymbole;
	public Sprite succesSymbole;

	public GameObject infoSymbole;
	public Text infoText;
	private string result;

	public GameObject buttonNext;
	public GameObject infoBox;

	void Update(){
		
	/*	result = isChecked (inputText.text);
		//Debug.Log (result);
		if (result == "Ok") {
			infoBox.GetComponent<Image> ().sprite = succesBoxBG;
			infoSymbole.GetComponent<Image> ().sprite = succesSymbole;
			infoText.text = "Valide : Pseudo Valide";
			pseudo = inputText.text;
			buttonNext.SetActive (true);
		}
		else {
			infoBox.GetComponent<Image> ().sprite = errorBoxBG;
			infoSymbole.GetComponent<Image> ().sprite = errorSymbole;
			infoText.text = result;
			buttonNext.SetActive (false);
		}*/

		if (!string.IsNullOrEmpty(inputText.text)) {
			pseudo = inputText.text;
			buttonNext.SetActive (true);
		}
	}

	string isChecked (string text) {
		nameAlreadyBDD (text);
		return result;
	}

	void nameAlreadyBDD (string text) {
		WWWForm formCheckName = new WWWForm ();
		formCheckName.AddField ("TypeRequest", "SelectName");
		formCheckName.AddField ("Pseudo", text);

		WWW pseudoTest = new WWW ("http://phenixos-lab.ddns.net/GysAPI/api.php", formCheckName);

		StartCoroutine (WaitForRequestCheckPseudo(pseudoTest));
	}

	public void addName(){
		WWWForm formAddName = new WWWForm ();
		formAddName.AddField ("TypeRequest", "AddName");
		formAddName.AddField ("Pseudo", pseudo);

		WWW pseudoAdd = new WWW ("http://phenixos-lab.ddns.net/GysAPI/api.php", formAddName);

		StartCoroutine (WaitForRequestAddPseudo (pseudoAdd));
	}

	IEnumerator WaitForRequestCheckPseudo(WWW pseudoTest) {
		yield return pseudoTest;

		if (!string.IsNullOrEmpty (pseudoTest.error)) {
			Debug.Log ("Erreur : Un problème est survenue lors de la connection");
		} else
			result = pseudoTest.text;
	}

	IEnumerator WaitForRequestAddPseudo (WWW pseudoAdd) {
		yield return pseudoAdd;

		Debug.Log (pseudoAdd.text);

		if (!string.IsNullOrEmpty (pseudoAdd.error)) {
			infoBox.GetComponent<Image> ().sprite = errorBoxBG;
			infoSymbole.GetComponent<Image> ().sprite = errorSymbole;
			infoText.text = "Erreur : Un problème est survenue lors de la connection";
		} else {
			PlayerPrefs.SetString ("pseudo", pseudo);
			GetComponent<ShowPanels> ().gameMenu.GetComponent<JouerMenu> ().StartNewGame ();
		}
	}

	public void launcheGame(){
		PlayerPrefs.SetString ("pseudo", pseudo);
		GetComponent<ShowPanels> ().gameMenu.GetComponent<JouerMenu> ().StartNewGame ();
	}
}
