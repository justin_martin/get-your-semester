﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour {

    public AudioMixer mainMixer;

    public Slider SliderMusique;
    public Slider SliderSFX;

    private static string PREF_MUSIC_SOUND = "musicVol";
    private static string PREF_SFX_SOUND = "sfxVol";

    private float DefaultSoundValue = 1f;

    void Awake()
    {
        // Définition du volume sauvegardé
        float musicVol = PlayerPrefs.HasKey(PREF_MUSIC_SOUND) ? PlayerPrefs.GetFloat(PREF_MUSIC_SOUND) : DefaultSoundValue;
        float sfxVol = PlayerPrefs.HasKey(PREF_SFX_SOUND) ? PlayerPrefs.GetFloat(PREF_SFX_SOUND) : DefaultSoundValue;

        SliderMusique.value = musicVol;
        SliderSFX.value = sfxVol;
    }

    // Modifie la valeur du volume du son (Appelé quand le slider a sa valeur modifié)
    public void SetMusicLevel(float musicLvl)
    {
        mainMixer.SetFloat(PREF_MUSIC_SOUND, musicLvl);
        PlayerPrefs.SetFloat(PREF_MUSIC_SOUND, musicLvl);
    }

    // Modifie la valeur du volume des effets (Appelé quand le slider a sa valeur modifié)
    public void SetSfxLevel(float sfxLevel)
    {
        mainMixer.SetFloat(PREF_SFX_SOUND, sfxLevel);
        PlayerPrefs.SetFloat(PREF_SFX_SOUND, sfxLevel);
    }
}