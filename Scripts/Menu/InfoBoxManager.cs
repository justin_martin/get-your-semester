﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoBoxManager : MonoBehaviour {

	public Text inputText;

	public Sprite errorBoxBG;
	public Sprite succesBoxBG;

	public Sprite errorSymbole;
	public Sprite succesSymbole;

	public GameObject infoSymbole;
	public Text infoText;
	private string result;

	public GameObject buttonNext;

	public string pseudo;

	// Update is called once per frame
	void Update () {
		
		result = isChecked (inputText.text);
		//Debug.Log (result);
		if (result == "Ok") {
			gameObject.GetComponent<Image> ().sprite = succesBoxBG;
			infoSymbole.GetComponent<Image> ().sprite = succesSymbole;
			infoText.text = "Valide : Pseudo Valide";
			pseudo = inputText.text;
			buttonNext.SetActive (true);
		}
		else {
			gameObject.GetComponent<Image> ().sprite = errorBoxBG;
			infoSymbole.GetComponent<Image> ().sprite = errorSymbole;
			infoText.text = result;
			buttonNext.SetActive (false);
		}
	}

	string isChecked (string text) {
			nameAlreadyBDD (text);
			return result;
	}

	void nameAlreadyBDD (string text) {
		
		WWWForm formCheckName = new WWWForm ();
		formCheckName.AddField ("TypeRequest", "SelectName");
		formCheckName.AddField ("Pseudo", text);

		WWW pseudoTest = new WWW ("http://192.168.1.29/GysAPI/api.php", formCheckName);
	
		StartCoroutine (WaitForRequest (pseudoTest));
	}

	IEnumerator WaitForRequest(WWW pseudoTest) {
		yield return pseudoTest;

		Debug.Log (pseudoTest.text);

		if (!string.IsNullOrEmpty (pseudoTest.error)) {
			result =  "Erreur : Un problème est survenue lors de la connection";
		} else
			result = pseudoTest.text;
	}


}