﻿using UnityEngine;
using System.Collections;

public class ShowPanels : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject optionMenu;
    public GameObject creditsMenu;
    public GameObject gameMenu;
	public GameObject playerMenu;

    // Affiche le menu des options
    public void ShowOptionsPanel()
	{
        mainMenu.SetActive(false);
        optionMenu.SetActive(true);
        creditsMenu.SetActive(false);
        gameMenu.SetActive(false);
		playerMenu.SetActive (false);
    }

	// Affiche le menu principale
	public void ShowMenu()
	{
        mainMenu.SetActive(true);
        optionMenu.SetActive(false);
        creditsMenu.SetActive(false);
        gameMenu.SetActive(false);
		playerMenu.SetActive (false);
	}
	
	// Affiche les crédits
	public void ShowCreditsPanel()
	{
        mainMenu.SetActive(false);
        optionMenu.SetActive(false);
        creditsMenu.SetActive(true);
		gameMenu.SetActive(false);
		playerMenu.SetActive (false);
    }

	public void ShowPlayerPanel()
	{
		mainMenu.SetActive(false);
		optionMenu.SetActive(false);
		creditsMenu.SetActive(false);
		gameMenu.SetActive(false);
		playerMenu.SetActive (true);
	}

    // Affiche le menu principale
    public void ShowJouerPanel()
    {
        mainMenu.SetActive(false);
        optionMenu.SetActive(false);
        creditsMenu.SetActive(false);
        gameMenu.SetActive(true);
		playerMenu.SetActive (false);
    }

	public void openTracker(){
		Application.OpenURL ("https://bitbucket.org/justin_martin/get-your-semester/issues");
	}
}