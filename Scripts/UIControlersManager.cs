﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
 * Created by Justin "PheniXos" Martin
 * Ce script est charger de gérer l'affichage du composant d'interface de contrôlle de téléphone
 * */

public class UIControlersManager {

	//4 touches directionnelles de l'interface
	[SerializeField]
	private GameObject upArrow;
	[SerializeField]
	private GameObject downArrow;
	[SerializeField]
	private GameObject leftArrow;
	[SerializeField]
	private GameObject rightArrow;

	//2 bouttons action de l'interface
	[SerializeField]
	private GameObject buttonA;
	[SerializeField]
	private GameObject buttonB;

	/**
	 * méthode permettant de rendre transparente l'interface
	 * @param value valeur de transparance a afficher
	 **/

	public void UITranparence(float value){
		Color c = upArrow.GetComponent<Image> ().color;
		c.a = value;

		//For each 
		upArrow.GetComponent<Image> ().color = c;
		downArrow.GetComponent<Image> ().color = c;
		rightArrow.GetComponent<Image> ().color = c;
		leftArrow.GetComponent<Image> ().color = c;

		buttonA.GetComponent<Image> ().color = c;
		buttonB.GetComponent<Image> ().color = c;
	}
}
