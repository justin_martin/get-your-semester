﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchSalle : MonoBehaviour {

	[SerializeField]
    private GameObject zoneSortie;
	[SerializeField]
    private GameObject player;
	[SerializeField]
	private GameObject fadeTp;

	[SerializeField] private bool isTo2ndStage;
	[SerializeField] private bool isTo1stStage;

	[SerializeField] private AudioClip sound2ndStage;
	[SerializeField] private AudioClip sound1stStage;




	void OnTriggerEnter2D(Collider2D collider) {
		SoundManager soundMan = FindObjectOfType<SoundManager> ();
		if(isTo1stStage) 
			soundMan.playSoundGlobal (sound1stStage);
		else if(isTo2ndStage)
			soundMan.playSoundGlobal (sound2ndStage);
		
		Vector3 destPos = zoneSortie.transform.position;
		StartCoroutine (fadeTp.GetComponent<Fade>().DoFadeWithPlayerMove(destPos.x, destPos.y));
	}
}