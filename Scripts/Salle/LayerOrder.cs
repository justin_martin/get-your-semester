﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerOrder : MonoBehaviour {

    public Transform player;
    public int layerBack = 4;
    public int layerFront = 5;

    SpriteRenderer rsprite;
    Transform elem;
    // Use this for initialization
    void Start () {
        rsprite = GetComponent<SpriteRenderer>();
        elem = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
        if (elem.position.y > player.position.y) rsprite.sortingOrder = layerBack;
        else rsprite.sortingOrder = layerFront;
	}
}
