using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System;
using System.Linq;

public class WinManager : MonoBehaviour {

	[HideInInspector] private bool winActive;

	public GameObject Fade;
	public GameObject player;

	public GameObject justin;
	public GameObject maeva;
	public GameObject jc;
	public GameObject walko;

	public GameObject endDialogueHolder;

	public void winState (bool state) {
		gameObject.SetActive (state);
		winActive = state;
	}
		
	public void launchEnd () {
		StartCoroutine (Fade.GetComponent<Fade> ().DoFadeWithPlayerMove (Constant.endVecteur.x, Constant.endVecteur.y, true));
		player.GetComponent<Animator> ().SetFloat ("input_x", 0);
		player.GetComponent<Animator> ().SetFloat ("input_y", -1);

		walko.SetActive (true);
		StartCoroutine (launchDialog());
		justin.SetActive (true);
		maeva.SetActive (true);
		jc.SetActive (true);
	}

	private IEnumerator launchDialog () {
		while (Fade.GetComponent<Fade> ().fadeActive)
			yield return new WaitForSeconds (0.1f);

		endDialogueHolder.GetComponent<DialogueHolder> ().activeDialog ();
	}
}
