﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DialogueHolder : MonoBehaviour {

	public bool isPresAdiil;
	public bool isVisiteActive;
	public bool gameOverDialogue;
	public bool isFightDialogue = false;
	public bool winGame;
	public bool isUsed;
	public bool isTuto;

	public TextAsset dialogueText;
	public GameObject dialogueBox;

	public GameObject ennemi;

    private string dialogueString;

    private PlayerStats pStats;
    private DialogueManager dMan;
    private ButtonListener buttonListener;
    private String[] dialogueLines;

    private bool isEnter = false;

    // Use this for initialization
    void Start() {    
		dMan = dialogueBox.GetComponent<DialogueManager>();
        buttonListener = FindObjectOfType<ButtonListener>();
		pStats = FindObjectOfType<PlayerStats>();
    }
	
	// Update is called once per frame
	void Update () {
		if ((!isUsed || isFightDialogue) && isEnter && !dMan.dialogActive && (buttonListener.GetButtonAClick () || Input.GetButtonDown("Dialoguer"))) {
			activeDialog ();
		}
		//buttonListener.DefineUIMobileState(dMan.dialogActive);
    }

    void OnTriggerEnter2D(Collider2D other) {
        isEnter = true;
    }

    void OnTriggerExit2D(Collider2D other) {
        isEnter = false;
		dMan.hideDialog();
    }
		
    void replaceInfo() {
		pStats = FindObjectOfType<PlayerStats>();
		dialogueString = dialogueString.Replace("[username]", pStats.pseudo);
    }

	public void activeDialog() {
		if (gameOverDialogue || isFightDialogue)
			isUsed = false;
		else
			isUsed = true;

		dMan = dialogueBox.GetComponent<DialogueManager>();
		if (dialogueText != null) {
			dialogueString = dialogueText.text;
			replaceInfo ();
			dialogueLines = dialogueString.Split (new[] { '\n' });
			if (isPresAdiil) {
				dMan.setPresLine (5);
				dMan.setPresActive (true);
			} else {
				dMan.setPresActive (false);
			}

			if (isVisiteActive) dMan.setVisiteActive (true);
			else dMan.setVisiteActive (false);
			 
			if (gameOverDialogue) dMan.setGameOverActive (true);
			else dMan.setGameOverActive (false);

			if (winGame) {
				pStats.hasEnded = true;
				dMan.setWinGameMode (true);
			}else dMan.setWinGameMode (false);

			if (isFightDialogue)
				dMan.setFightMode (true, ennemi);
			else
				dMan.setFightMode (false, null);
		}

		dMan.dialogLines = dialogueLines;
		dMan.showDialog();
	}
}