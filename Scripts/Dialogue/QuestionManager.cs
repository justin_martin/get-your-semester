﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour {

	public static bool questionActive;
	public Sprite baseButtonSprite;
	public Sprite selectedButtonSprite;

	[SerializeField] private TextAsset[] textQuestionData;
	public Dictionary<string, TextAsset> textQuestion;
	public Text questionText;

	public GameObject[] sousMenu;
	private bool q1Active;
	private bool q2Active;
	private bool q3Active;

	private int indexSelected;
	private float waitTime = 0.01f;

	public GameObject pseudoLine;
	public GameObject narraMan;

	private ButtonListener buttonListener;

	private Coroutine actualCoroutine;
	private string actualText;
	private bool isLoading =false;

	// Use this for initialization
	void Start () {	}
	
	// Update is called once per frame
	void Update () { 
		if (q1Active && questionActive && (Input.GetKeyDown(KeyCode.DownArrow) || buttonListener.GetDownArrowClick())) {
			setButtonState (sousMenu [indexSelected], false);
			if (indexSelected == sousMenu.GetLength (0) - 1)
				indexSelected = 0;
			else 
				indexSelected++;
			setButtonState (sousMenu [indexSelected], true);
		}

		if (q1Active && questionActive && (Input.GetKeyDown(KeyCode.UpArrow) || buttonListener.GetUpArrowClick())) {
			setButtonState (sousMenu [indexSelected], false);
			if (indexSelected == 0)
				indexSelected = sousMenu.GetLength(0)-1;
			else
				indexSelected--;
			setButtonState (sousMenu [indexSelected], true);
		}
						
		if (q1Active && questionActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) {
			gameObject.SetActive (false);
			q1Active = false;
			questionActive = false;
			narraMan.GetComponent<NarrateurManager> ().launchPart2 (sousMenu[indexSelected]);
		}
		if (q2Active && questionActive && (Input.GetKeyDown(KeyCode.Return)|| buttonListener.GetButtonAClick())) {
			gameObject.SetActive (false);
			q2Active = false;
			questionActive = false;
			narraMan.GetComponent<NarrateurManager> ().launchPart3 ();
		}

		if (q3Active && questionActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) {
			gameObject.SetActive (false);
			q3Active = false;
			questionActive = false;
			narraMan.GetComponent<NarrateurManager> ().launchPart4 ();
		}

		if (isLoading && (Input.GetKeyDown (KeyCode.Return) || Input.touchCount > 0)) {
			StopCoroutine (actualCoroutine);
			#if UNITY_ANDROID
			buttonListener.ShowUIMobile ();
			#endif
			questionText.text = actualText;
		}
	}

	private IEnumerator waitTouch() {
		yield return new WaitForSeconds (0.5f);
	}

	public void StateQuestion1(bool state) {
		q1Active = state;
		indexSelected = 0;
		questionActive = state;
		gameObject.SetActive (state);
		init ();

		actualCoroutine = StartCoroutine (LoadLetters(textQuestion["first"].text));
		setButtonState (sousMenu[indexSelected], true);
	}

	public void StateQuestion2 (bool state, string sexe, string pseudo) {
		q2Active = state;
		questionActive = state;
		gameObject.SetActive (state);
		init ();
		actualCoroutine = StartCoroutine (LoadLetters (textQuestion[sexe].text));
	
		pseudoLine.GetComponent<InputField> ().text = pseudo;
		setButtonState (sousMenu [0], true);
	}

	public void StateQuestion3 (bool state) {
		q3Active = state;
		questionActive = state;
		gameObject.SetActive (state);
		init ();
		actualCoroutine = StartCoroutine (LoadLetters (textQuestion ["first"].text));
		setButtonState (sousMenu [0], true);
	}

	void setButtonState (GameObject button, bool isSelected) {
		if (isSelected)
			button.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			button.GetComponent<Image> ().sprite = baseButtonSprite;
	}
		
	private IEnumerator LoadLetters(string completeText) {
		isLoading = true;
		actualText = completeText;
		int textSize = 0;
		while (textSize < completeText.Length) {
			questionText.text += completeText[textSize++];
			yield return new WaitForSeconds(waitTime);
		}
		isLoading = false;
	}

	private void init(){
		textQuestion = new Dictionary<string, TextAsset> ();

		if (textQuestionData.Length == 3) {
			textQuestion.Add ("Homme", textQuestionData [0]);
			textQuestion.Add ("Femme", textQuestionData [1]);
			textQuestion.Add ("Alien", textQuestionData [2]);
		} else  {
			Debug.Log (gameObject.name);
			textQuestion.Add ("first", textQuestionData [0]);
		}
		buttonListener = FindObjectOfType<ButtonListener>();
	}
}
	