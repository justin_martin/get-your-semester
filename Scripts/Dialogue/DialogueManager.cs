﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System;
using System.Linq;

public class DialogueManager : MonoBehaviour {

    public Text dTexte;

	public GameObject spaceToContinue;
	public GameObject[] membersAdiil;
	public GameObject visiteBox;
	public GameObject gameOverBox;
	public GameObject winBox;
	public GameObject fightBox;

	private GameObject EnnemiFight;

	public string[] dialogLines;
	public bool dialogActive;

	public static bool dialogueActive;

	public GameObject cameras;
	public GameObject fade;

	private int currentLine = 0;
	private int previousLine = -1;

	private float waitTime = 0.001f;

	private bool setPresMode;
	private bool isPresActive;
	private int linePresMode;
	private bool isGameOverMode;
	private	bool isWinMode;
	private bool isFightMode = false;
	private bool isBeginMode;

	private bool isVisiteActive;
	private int indexAdiil;

	private ButtonListener buttonListener;
	public GameObject Controler;

	private Coroutine actualCoroutine;
	private bool loadingText;
	private string actualText;

	void Start () {
		buttonListener = FindObjectOfType<ButtonListener>();
	}

    void Update() {

		if (dialogActive && (Input.GetKeyDown (KeyCode.Return) || Input.touchCount>0) && !loadingText)
			currentLine++;

		if (dialogActive && (Input.GetKeyDown (KeyCode.Return) || Input.touchCount > 0) && loadingText) {
			skipLine ();
			#if UNITY_ANDROID 
				StartCoroutine(waitTouch());
			#endif
		}	
		
		if(dialogActive && currentLine >= dialogLines.Length)
			hideDialog();
		else if(dialogActive && previousLine != currentLine) {
			if (currentLine == linePresMode && isPresActive) {
				setPresMode = true;
				indexAdiil = 0;
			}
			if (setPresMode) {
				StartCoroutine(fade.GetComponent<Fade> ().DoFadeWithMovCam (membersAdiil [indexAdiil].transform, false));
				indexAdiil++;
				if (indexAdiil ==7) {
					setPresMode = false;
					StartCoroutine(fade.GetComponent<Fade> ().DoFadeFollowPlayer(true));
				}
			}

			actualCoroutine = StartCoroutine (LoadLetters (dialogLines [currentLine]));
			previousLine = currentLine;
        }
               
    }

    public void showDialog() {
        dialogActive = true;
		dialogueActive = true;
		currentLine = 0;
		gameObject.SetActive(true);
    }

    public void hideDialog() {
		if (dialogActive) {
			dialogLines = null;
			currentLine = 0;
			previousLine = -1;
			dialogActive = false;
			dialogueActive = false;

			#if UNITY_ANDROID
				buttonListener.ShowUIMobile ();
			#endif

			if (isVisiteActive)	endVisiteDialogue ();
			else if (isGameOverMode) endGameOverDialogue ();
			else if (isWinMode)	endWinDialogue ();
			else if (isFightMode) endFightDialogue ();
			else if (isPresActive) endPresDialogue ();
			else gameObject.SetActive (false);

		}
    }

	private void endVisiteDialogue (){
		StartCoroutine (fade.GetComponent<Fade> ().DoFadeWithPlayerMove (Constant.examVecteur.x, Constant.examVecteur.y, true));
		StartCoroutine (returnVisite ());	
	}

	private void endGameOverDialogue (){
		StartCoroutine (fade.GetComponent<Fade> ().DoFadeWithPlayerMove (6.4f, -22.28f, true));
		gameObject.SetActive (false);
		gameOverBox.GetComponent<GameOverManager> ().gameOverState (true);
	}

	private void endWinDialogue (){
		winBox.GetComponent<WinManager> ().winState (true);
		winBox.GetComponent<WinManager> ().launchEnd ();
		gameObject.SetActive (false);
	}

	private void endFightDialogue(){
		isFightMode = false;
		fightBox.GetComponent<FightManager> ().setEnnemi (EnnemiFight);
		fightBox.GetComponent<FightManager> ().StateFight (true);
		cameras.GetComponent<CameraFollow> ().setPositionCam ((float)-8.06, (float)-4.55, (float)-10);
		gameObject.SetActive (false);
	} 

	private void endPresDialogue () {
		disableADIIL();
		visiteBox.GetComponent<VisiteManager> ().StateVisite (true);

	}

	private void disableADIIL (){
		foreach (GameObject g in membersAdiil)
			g.SetActive (false);
	}

	private IEnumerator returnVisite() {
		while (fade.GetComponent<Fade> ().fadeActive)
			yield return new WaitForSeconds (0.1f);

		visiteBox.GetComponent<VisiteManager> ().StateVisite (true);
		gameObject.SetActive(false);
	}
		
	private IEnumerator waitTouch() {
		yield return new WaitForSeconds (0.5f);
	}

	private IEnumerator LoadLetters(string completeText) {
		actualText = completeText;
		waitTime = 0.001f;
		loadingText = true;
		actualText = completeText;
		spaceToContinue.SetActive(false);

		dTexte.text = "";

		int textSize = 0;
		while (textSize < completeText.Length) {
			dTexte.text += completeText[textSize++];
			yield return new WaitForSeconds (waitTime);
		}
			
		spaceToContinue.SetActive (true);
		loadingText = false;
	}

	public void setPresLine(int nbLine) {
		linePresMode = nbLine;

	}

	public void setPresActive(bool state) {
		isPresActive = state;
		/*foreach (GameObject g in membersAdiil) {
			g.GetComponent<IAMove>().canMove (true);
		}*/
	}

	public void setVisiteActive (bool state) {
		isVisiteActive = state;
	}

	public void setGameOverActive(bool state) {
		isGameOverMode = state;
	}

	public void setWinGameMode (bool state)
	{
		isWinMode = state;
	}

	public void setFightMode (bool state, GameObject Ennemi) {
		isFightMode = state;
		EnnemiFight = Ennemi;
	}

	public void setEnnemi(GameObject ennemi){
		this.EnnemiFight = ennemi;
	}

	void skipLine () {
		StopCoroutine(actualCoroutine);
		dTexte.text = actualText;
		loadingText = false;
		spaceToContinue.SetActive (true);
	}
}	
