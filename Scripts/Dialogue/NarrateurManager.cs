﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NarrateurManager: MonoBehaviour {

	public TextAsset[] text;

	public Text textBox;

	public GameObject textNext;

	public GameObject commandeBox;

	public GameObject QuestionBox1;
	public GameObject QuestionBox2;
	public GameObject QuestionBox3;

	public GameObject zzzEffect;

	public AudioClip Ah_File;

	public static bool introActive;

	private float waitTime = 0.01f;
	private int tActive = 0;

	bool isLoading = false;
	bool isPart1 = false;
	bool isPart4 = false;

	public Sprite defaultSprite;
	public GameObject player;
	public GameObject camera;
	public GameObject Fade;
	ButtonListener buttonListener;
	public GameObject plane;

	private string actualText;
	private Coroutine actualCoroutine;
	// Use this for initialization
	void Start () {
		buttonListener = FindObjectOfType<ButtonListener>();
		//player.GetComponent<Animator> ().enabled = false;
		//player.GetComponent<SpriteRenderer> ().sprite = defaultSprite;
	}

	public void StateIntro(bool state){
		tActive = 0;
		gameObject.SetActive (true);
		introActive = true;
		camera.GetComponent<CameraFollow> ().setPositionCam (29.45f,-11.86f,-10f);
		player.transform.position = new Vector3(29.45f, -10.40f, -0.2f);
		commandeBox.SetActive (true);
		commandeBox.GetComponent<CommandeManager> ().useTuto = true;
		commandeBox.GetComponent<CommandeManager> ().StateCommande (true);
	}

	// Update is called once per frame
	void Update () {

		if (commandeBox.GetComponent<CommandeManager> ().commandeActive && introActive && (Input.GetKeyDown(KeyCode.Escape) || buttonListener.GetButtonBClick())) {
			commandeBox.GetComponent<CommandeManager> ().StateCommande (false);
			launchPart1 ();
		}

		if (introActive && isPart1 && !isLoading) {
			textNext.SetActive (true);
		}
			
		if (introActive && isPart1 && !isLoading && (Input.GetKeyDown(KeyCode.Return)|| Input.touchCount>0)) {
			textNext.SetActive (false);
			textBox.gameObject.SetActive (false);
			QuestionBox1.GetComponent<QuestionManager> ().StateQuestion1(true);
			isPart1 = false;
		}


		if (introActive && isPart4 && !isLoading) {
			textNext.SetActive (true);
		}

		if (introActive && isPart4 && !isLoading && (Input.GetKeyDown(KeyCode.Return) || Input.touchCount>0)) {
			textNext.SetActive (false);
			textBox.gameObject.SetActive (false);
			zzzEffect.SetActive (false);
			plane.SetActive (false);

			isPart4 = false;
			introActive = false;

			StartCoroutine(Fade.GetComponent<Fade>().DoFadeWithPlayerMove(Constant.examVecteur.x, Constant.examVecteur.y, true));
		}

		if(isLoading && (Input.GetKeyDown(KeyCode.Return)||Input.touchCount>0)){
			StopCoroutine(actualCoroutine);
			textBox.text = actualText;
			isLoading = false;
			#if UNITY_ANDROID 
			StartCoroutine(waitTouch());
			#endif
		}
	}

	private IEnumerator waitTouch() {
		yield return new WaitForSeconds (0.5f);
	}

	void launchPart1 () {
		isPart1 = true;
		actualCoroutine = StartCoroutine (LoadLetters (text[tActive].text));
		tActive++;
	}
		
	public void launchPart2 (GameObject button) {
		gameObject.SetActive (true);
		introActive = true;
		SoundManager soundMan = FindObjectOfType<SoundManager> ();
		if (button.name == "Alien")
			soundMan.playSoundEffect(Ah_File);
		
		player.GetComponent<PlayerStats> ().setPlayerSprite (button.name);
		player.GetComponent<Animator> ().enabled = true;
		QuestionBox2.GetComponent<QuestionManager>().StateQuestion2(true, button.name, player.GetComponent<PlayerStats>().pseudo);
	}

	public void launchPart3() {
		gameObject.SetActive (true);
		introActive = true;
		QuestionBox3.GetComponent<QuestionManager>().StateQuestion3(true);
	}

	public void launchPart4 () {
		gameObject.SetActive (true);
		textBox.gameObject.SetActive (true);

		introActive = true;
		isPart4 = true;

		StartCoroutine(Fade.GetComponent<Fade>().DoFadeWithPlayerMove(Constant.introVecteur.x, Constant.introVecteur.y, Constant.introCamerasVecteur.x, Constant.introCamerasVecteur.y));

		textBox.text = "";
		actualCoroutine = StartCoroutine(LoadLetters(text[tActive].text));

		plane.SetActive (true);
		zzzEffect.SetActive (true);
		tActive++;
	}
		
	private IEnumerator LoadLetters(string completeText) {
		actualText = completeText;
		isLoading = true;

		int textSize = 0;
		while (textSize < completeText.Length) {
			textBox.text += completeText[textSize++];
			yield return new WaitForSeconds(waitTime);
		}
		isLoading = false;
	}
}
