﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem {

	public string name;
	public Sprite gemmeSprite;

	public Gem(string nameArg){
		name = nameArg;
	}
}
