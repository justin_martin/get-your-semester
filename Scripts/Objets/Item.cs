﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item{

	private int id;
    private string nom;
	private string description;
	private int hpBonus;
	private int manaBonus;
	private int armorBonus;
	private int attaqueBonus;

	private PlayerStats pStats;

	public Item(int id, string nom, string description, int hpBonus, int manaBonus, int armorBonus, int attaqueBonus) {
		this.id = id;
        this.nom = nom;
		this.description = description;
		this.armorBonus = armorBonus;
		this.hpBonus = hpBonus;
		this.manaBonus = manaBonus;
		this.attaqueBonus = attaqueBonus;
    }

	public string getNom(){
		return nom;
	}

	public string getDescription(){
		return description;
	}

	public int getHpBonus(){
		return hpBonus;
	}

	public int getArmorBonus(){
		return armorBonus;
	}

	public int getManaBonus(){
		return manaBonus;
	}

	public int getAttaqueBonus(){
		return attaqueBonus;
	}

	public int getId(){
		return id;
	}
}