﻿public class Capacity{

	public string nom, description;
	public float minPercent, maxPercent;
	public int manaCost;

	public Capacity(string nom, string description, float minPercent, float maxPercent, int manaCost) {
		this.nom = nom;
		this.description = description;
		this.minPercent = minPercent;
		this.maxPercent = maxPercent;
		this.manaCost = manaCost;
	}
}
