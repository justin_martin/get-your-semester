﻿/**
 * Create by Justin "PheniXos" Martin
 * Ce script permet de tester les fonctionnalitées du jeu liées au capacité des enseignants
 * Utilisable dans l'ancienne version de combat
 * */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemCore : MonoBehaviour {

	Dictionary<int, Item> itemAll;
	[SerializeField] private AudioClip sound1stStage;
	private PlayerStats pStats;
	// Use this for initialization
	void Start () {
		SoundManager soundMan = FindObjectOfType<SoundManager> ();
		soundMan.playSoundGlobal (sound1stStage);
	}

	public void init(){
		itemAll = new Dictionary<int, Item> ();

		Item i = new Item (1, "Hot-dog", "un hot-dog régénrant", 3, 2, 0, 0);
		itemAll.Add(i.getId(), i);

		Item i1 = new Item (2, "Hot-chat", ":D", 0, 0, 3, 2);
		itemAll.Add(i1.getId(), i1);
	}

	public Item getItem (int id){
		Item item;
		itemAll.TryGetValue(id, out item);
		return item;
	}


	public void saveGame () {
		pStats = FindObjectOfType<PlayerStats> ();

		PlayerPrefs.DeleteAll ();
		PlayerPrefs.SetInt ("SavedGame", 1);

		PlayerPrefs.SetString ("Pseudo", pStats.pseudo);
		PlayerPrefs.SetString ("Sexe", pStats.sexe);

		saveGemme ();
		saveInvetaire ();
		saveStats ();
	}


	void saveGemme () {
		string gem = "";
		int i = 0;
		foreach (bool b in pStats.gemmes) {
			if (b)
				gem += i + "/";
			i++;
		}

		if (gem == "")
			gem = "NoGemme";
		PlayerPrefs.SetString ("Gemme", gem);
	}

	void saveInvetaire () {
		string inv = "";
		Item[] itemTab = new Item[pStats.inventaire.Count];
		pStats.inventaire.Keys.CopyTo (itemTab, 0);

		foreach (Item i in itemTab) {
			int nb;
			pStats.inventaire.TryGetValue (i, out nb);
			inv += i.getId () + "*" + nb + "/";
		}

		if (pStats.inventaire.Count == 0)
			inv = "NoItem";
		PlayerPrefs.SetString ("Inventaire", inv);
	}

	void saveStats () {

		PlayerPrefs.SetInt ("healthMax", pStats.healthMax);

		PlayerPrefs.SetInt ("lvl", pStats.lvl);
		PlayerPrefs.SetInt ("health", pStats.health);
		PlayerPrefs.SetInt ("palierXP", pStats.palierXp);
		PlayerPrefs.SetInt ("xp", pStats.xp);
		if (pStats.hasEnded)
			PlayerPrefs.SetInt ("hasEnded", 1);

		if (pStats.isLive)
			PlayerPrefs.SetInt ("isLive", 1);
		else
			PlayerPrefs.SetInt ("isLive", 0);

		PlayerPrefs.SetFloat ("playerPositionX", pStats.gameObject.transform.position.x);
		PlayerPrefs.SetFloat ("playerPositionY", pStats.gameObject.transform.position.y);
		PlayerPrefs.SetFloat ("playerPositionZ", pStats.gameObject.transform.position.z);
	}
}
