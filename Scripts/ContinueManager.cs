﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContinueManager : MonoBehaviour {


	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID || UNITY_IOS
			GetComponent<Text>().text = "Press Screen";
		#else 
			GetComponent<Text>().text = "Press Enter";

		#endif
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}