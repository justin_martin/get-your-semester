﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {

    private Rigidbody2D rbody;
    public Animator anim;

    private float speedMove = 1.0f;

    private bool moveTop;
    private bool moveBottom;
    private bool moveLeft;
    private bool moveRight;

    private bool controlByUI = false;

    // Use this for initialization
    void Start() {
        rbody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update() {
		if (MenuManager.inventaireActive || NarrateurManager.introActive || QuestionManager.questionActive || FightManager.fightActive || DialogueManager.dialogueActive || VisiteManager.visiteActive ||GameOverManager.gameOverActive || ResponseManager.responseActive)
        {
            rbody.constraints = RigidbodyConstraints2D.FreezeAll;
			setAnim (false);
        }
        else
        {
            rbody.constraints = RigidbodyConstraints2D.FreezeRotation;

			setAnim (true);

            #if UNITY_STANDALONE || UNITY_EDITOR
                CheckKeyboardPress();
            #endif

            if (moveTop) {
                rbody.MovePosition(rbody.position + new Vector2(0, speedMove * Time.deltaTime));
                anim.SetFloat("input_x", 0);
                anim.SetFloat("input_y", 1);
            }
            else if (moveBottom) {
                rbody.MovePosition(rbody.position + new Vector2(0, -speedMove * Time.deltaTime));
                anim.SetFloat("input_x", 0);
                anim.SetFloat("input_y", -1);
            }
            else if (moveLeft) {
                rbody.MovePosition(rbody.position + new Vector2(-speedMove * Time.deltaTime, 0));
                anim.SetFloat("input_x", -1);
                anim.SetFloat("input_y", 0);
            }
            else if (moveRight) {
                rbody.MovePosition(rbody.position + new Vector2(speedMove * Time.deltaTime, 0));
                anim.SetFloat("input_x", 1);
                anim.SetFloat("input_y", 0);
            }
            else {
				setAnim (false);
            }
        }

    }

    public void OnTopPressed(bool value)
    {
        moveTop = value;
        controlByUI = value;
    }

    public void OnBottomPressed(bool value)
    {
        moveBottom = value;
        controlByUI = value;
    }

    public void OnLeftPressed(bool value)
    {
        moveLeft = value;
        controlByUI = value;
    }

    public void OnRightPressed(bool value)
    {
        moveRight = value;
        controlByUI = value;
    }

    public void CheckKeyboardPress()
    {
        if (!controlByUI)
        {
            moveTop = false;
            moveBottom = false;
            moveLeft = false;
            moveRight = false;
        }

        float vertical = Input.GetAxisRaw("Vertical");
        float horizontal = Input.GetAxisRaw("Horizontal");

        if (vertical > 0)
            moveTop = true;
        else if (vertical < 0)
            moveBottom = true;
        else if (horizontal < 0)
            moveLeft = true;
        else if (horizontal > 0)
            moveRight = true;

    }

	public void setAnim (bool state) {
		anim.SetBool ("isWalking", state);
	}


}
