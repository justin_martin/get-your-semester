﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
public class PlayerStats : MonoBehaviour {

	public GameObject narrateur;
	public string sexe;
	public bool hasEnded;

	public Dictionary<string, RuntimeAnimatorController> anim;
	public Dictionary<string, Sprite> basePosition;
	public Dictionary<string, Sprite> avatar;

	[SerializeField] private Sprite[] avatarData; 
	[SerializeField] private Sprite[] basePositionData;
	[SerializeField] public RuntimeAnimatorController[] animData;

	public bool[] gemmes;
    
	public int healthPerLvl; // ratio de vie / level

	public int lvl;
    public int xp;
    public string pseudo;

	[HideInInspector] public bool isLive;
	[HideInInspector] public int healthMax;

    public int health;
    public int palierXp;
	public Sprite AvatarFace;
	private SystemCore systemCore;

	public Dictionary<Item, int> inventaire;
	private Dictionary<string,int> gemmesIndex;
    // Use this for initialization
    void Start () {
		systemCore = FindObjectOfType<SystemCore> ();
		anim = new Dictionary<string, RuntimeAnimatorController>();
		basePosition = new Dictionary<string, Sprite>();
		avatar = new Dictionary<string, Sprite>();


		anim.Add ("Homme", animData [0]);
		anim.Add ("Femme", animData [1]);
		anim.Add ("Alien", animData [2]);

		basePosition.Add ("Homme", basePositionData [0]);
		basePosition.Add ("Femme", basePositionData [1]);
		basePosition.Add ("Alien", basePositionData [2]);

		avatar.Add ("Homme", avatarData [0]);
		avatar.Add ("Femme", avatarData [1]);
		avatar.Add ("Alien", avatarData [2]);

		healthMax = health;

		palierXp = 4;

		inventaire = new Dictionary<Item, int> ();
		gemmesIndex = new Dictionary<string, int> ();

		gemmesIndex.Add ("Icône_Rubis_Black_BotW", 0);
		gemmesIndex.Add ("Icône_Rubis_Rose_BotW",1);
		gemmesIndex.Add ("Icône_Rubis_Rouge_BotW",3);
		gemmesIndex.Add ("Icône_Rubis_Doré_BotW",2);
		gemmesIndex.Add ("Icône_Rubis_Argenté_BotW",8);
		gemmesIndex.Add ("Icône_Rubis_Vert_BotW",5);
		gemmesIndex.Add ("Icône_Rubis_Violet_BotW",6);
		gemmesIndex.Add ("Icône_Rubis_LightBleu_BotW",4	);
		gemmesIndex.Add ("	", 7);
		
		systemCore.init ();

		hasEnded = false;
		isLive = true;
		//		Debug.Log(inventaire[i1];

		if (PlayerPrefs.HasKey ("startNewGame")) {
			if(PlayerPrefs.HasKey("pseudo")) {
				pseudo = PlayerPrefs.GetString("pseudo");
				PlayerPrefs.DeleteKey("pseudo");
			}
			narrateur.GetComponent<NarrateurManager> ().StateIntro (true);
			PlayerPrefs.DeleteAll();
		}
		if (PlayerPrefs.HasKey ("resumeGame")) {
			extractSave ();
			PlayerPrefs.DeleteAll();
		}
			
	}
	
	// Update is called once per fram	e
	void Update () {
		if (Input.GetKeyDown (KeyCode.CapsLock))
			PlayerPrefs.DeleteAll ();
	}

	/**
	 * fonction lvlUp permettant de definir la nouvelle vie, et le nouvelle objectif d'xp à atteindre
	 * */
    public void lvlUp() {
        lvl++;
        palierXp = palierXp * 2;
       
		health = healthPerLvl * lvl;
		healthMax = health;
    }


	/**
	 * Ajoute de l'xp, test si le joueur a atteint le palier xp le fait lvlUp
	 * */
    public void addXp(int xpToAdd) {
        xp += xpToAdd;
        if(xp > palierXp) {
            lvlUp();
        }
    }

	/**
	 * Ajoute un item à l'inventaire du joueur
	 * */
    public void addItem(Item i) {
		if (inventaire.ContainsKey (i))
			inventaire [i]++;
		else
			inventaire.Add (i, 1);
    }

	/**
	 * Supprime un item à l'inventaire du joueur
	 * @param i item à supprimer
	 * */
    public void removeItem(Item i){
		if (inventaire [i] > 1)
			inventaire [i]--;
		else
			inventaire.Remove (i);
    }

	public void addGemme(Sprite spriteGemme) {
		gemmes [gemmesIndex [spriteGemme.name]] = true;
	}

	public bool checkGemme(Sprite spriteGemme){
		return gemmes [gemmesIndex [spriteGemme.name]];
	}

	public void setPlayerSprite (string sexe) {
		Debug.Log (sexe);
		this.sexe = sexe;

		gameObject.GetComponent<Animator> ().runtimeAnimatorController = anim [sexe];
		AvatarFace = avatar [sexe];
		gameObject.GetComponent<SpriteRenderer> ().sprite = basePosition [sexe];
	}

	public bool verifMana (int Mana) {
		return true;
	}
		
	public void sufferDamage(int damage) {
		health -= damage;
		if (health <= 0)
			isLive = false;
	}

	public void useItem (Item objetSelected) {
		health += objetSelected.getHpBonus ();
	
		Debug.Log (health + " ******** " + healthMax);
		/*depassement par le haut */
		if (health < 0)  //cas impossible
			health = 0;
		
		/*depassement par le bas*/
		if (health > healthMax)
			health = healthMax;
		
		removeItem (objetSelected);
		
	}

	public bool hadAllGemme () {
		foreach (bool b in gemmes) {
			if (!b)
				return false;
		}
		return true;
	}

	void extractSave () {
		if (PlayerPrefs.GetInt ("SavedGame") == 1) {
			pseudo = PlayerPrefs.GetString ("Pseudo");
			setPlayerSprite (PlayerPrefs.GetString ("Sexe"));

			extractGemme ();
			extractInventaire ();
			extractStats ();
		}
	}

	void extractGemme (){
		string toExtract = PlayerPrefs.GetString ("Gemme");
		if (toExtract == "NoGemme")
			return;
		
		toExtract = toExtract.Substring(0, toExtract.Length-1);
		string[] toExtractTab = toExtract.Split ('/');

		foreach(string gemmeIndexString in toExtractTab){
			Debug.Log (gemmeIndexString);
			int index = Int32.Parse (gemmeIndexString);
			gemmes [index] = true;
		}
	}

	void extractInventaire () {
		string toExtract = PlayerPrefs.GetString ("Inventaire");
		if (toExtract == "NoItem")
			return;
		
		toExtract = toExtract.Substring(0, toExtract.Length-1);
		string[] toExtractTab = toExtract.Split ('/');

		foreach (String itemData in toExtractTab) {
			string[] itemDataTab = itemData.Split ('*');
			int idItem = Int32.Parse (itemDataTab [0]);
			Item item = systemCore.getItem (idItem);
			int nbItem = Int32.Parse (itemDataTab [1]);
			inventaire.Add (item, nbItem);
		}
			
	}



	void extractStats () {
		healthMax = PlayerPrefs.GetInt ("healthMax");

		lvl = PlayerPrefs.GetInt ("lvl");
		health = PlayerPrefs.GetInt ("health");
		palierXp = PlayerPrefs.GetInt ("palierXP");
		xp = PlayerPrefs.GetInt ("xp");
		//hasEnded = PlayerPrefs.HasKey ("hasEnded");
		isLive = PlayerPrefs.GetInt ("isLive") == 1 ? true : false;

		float newX =  PlayerPrefs.GetFloat ("playerPositionX");
		float newY =  PlayerPrefs.GetFloat ("playerPositionY");
		float newZ =  PlayerPrefs.GetFloat ("playerPositionZ");

		gameObject.transform.position = new Vector3 (newX, newY, newZ);
	}

	public Sprite getSprite () {
		return basePosition [sexe];
	}
}
