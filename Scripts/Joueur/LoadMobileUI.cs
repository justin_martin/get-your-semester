﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMobileUI : MonoBehaviour {

    public GameObject control_ui;

	// Use this for initialization
	void Update(){
		#if UNITY_IOS || UNITY_ANDROID || UNITY_WP_8_1 || UNITY_EDITOR
			if(!DialogueManager.dialogueActive){
				control_ui.SetActive(true);
			} else {
				control_ui.SetActive(false);
			}
		#else
		control_ui.SetActive(false);
		#endif
	}
}
