﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
	private Transform defaultTarget;

	private bool followActive = true;
	// Update is called once per frame

	void Start() {
		defaultTarget = target;
	}

	void Update () {
		if (followActive) {
			Vector3 distanceHauteurEcart = new Vector3 (0, 0, -10);
			transform.position = target.position + distanceHauteurEcart;
		}
	}

	public void setPositionCam(float x, float y, float z){
		followActive = false;
		transform.position = new Vector3 (x, y, z);
	}

	public void setFollowActive(bool state){
		followActive = state;
	}

	public void setFollowTarget(Transform target1) {
		target = target1;
		setFollowActive (true);
	}

	public void setDefaultFollowTarget() {
		setFollowTarget(defaultTarget);
	}
		
}
