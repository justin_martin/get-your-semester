﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResponseManager : MonoBehaviour {

	[SerializeField] private GameObject fightBox;
	[SerializeField]private GameObject questionBox;

	[SerializeField]private GameObject[] sousMenu;
	private int indexSelected;

	[SerializeField]private Sprite selectedButtonSprite;
	[SerializeField]private Sprite baseButtonSprite;
	[SerializeField]private Sprite disabledButtonSprite;

	[SerializeField]private AudioClip goodAnswerSound;
	[SerializeField]private AudioClip wrongAnswerSound;

	SoundManager soundMan;
	ButtonListener buttonListener;
	private QuestionBoxManager questionBoxMan;
	private FightManager fightMan;


	[HideInInspector] public static bool responseActive = false;

	// Use this for initialization
	void Start () {
		questionBoxMan = questionBox.GetComponent<QuestionBoxManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (responseActive && (Input.GetKeyDown(KeyCode.RightArrow) || buttonListener.GetRightArrowClick())) { //Si la touche de direction bas est appuyée (PC ou UI Mobile)
			setButtonState (sousMenu [indexSelected], false); //desactivation du bouton actuel
			if (indexSelected == sousMenu.GetLength(0)-1) indexSelected = 0; //si on est au bouton le plus bas on selectionne le premier
			else indexSelected++; //sinon le suivant
			setButtonState(sousMenu[indexSelected], true); //on active ce bouton
		}

		if (responseActive && (Input.GetKeyDown(KeyCode.LeftArrow) ||  buttonListener.GetLeftArrowClick())) { //Si la touche de direction bas est appuyée (PC ou UI Mobile)			Debug.Log (indexSelected+ " --- " + sousMenu.Length);
			setButtonState (sousMenu [indexSelected], false);
			if (indexSelected == 0) indexSelected = sousMenu.GetLength(0)-1;
			else indexSelected--;
			setButtonState (sousMenu[indexSelected], true);
		}


		if (responseActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) { //Si le bouton A ou la touche entrer est appuyé
			if (questionBoxMan.checkAnswer (sousMenu [indexSelected].name)) {
				soundMan.playSoundEffect(goodAnswerSound);
				fightBox.GetComponent<FightManager> ().loseHpProf ();
			} else {
				soundMan.playSoundEffect(wrongAnswerSound);
				fightBox.GetComponent<FightManager> ().loseHpPlayer();
			}
		}
	}
	

	public void responseState(bool state) {
		buttonListener = FindObjectOfType<ButtonListener> ();
		soundMan = FindObjectOfType<SoundManager> ();
		gameObject.SetActive (state);
		responseActive = state;
		indexSelected = 0;
		cleanButtonState ();
		setButtonState (sousMenu [indexSelected], true);
	}

	public void cleanButtonState(){
		foreach (GameObject g in sousMenu) {
			setButtonState (g, false);
		}
	}

	void setButtonState (GameObject buttonSelected, bool state) {
		if (state)
			buttonSelected.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			buttonSelected.GetComponent<Image> ().sprite = baseButtonSprite;
	}
		

	void disableSelectedButton () { 
		sousMenu [indexSelected].GetComponent<Image> ().sprite = disabledButtonSprite;
		if(sousMenu.GetLength (0) == 1) sousMenu = null;
		else {
			GameObject[] sousMenuTemp = new GameObject[sousMenu.GetLength (0)-1];
			int cpt = 0;
			int cpt_temp = 0;
			foreach (GameObject s in sousMenu) {
				if (cpt != indexSelected) {
					sousMenuTemp [cpt_temp] = sousMenu [cpt];
					cpt_temp++;
				}
				cpt++;
			}
			sousMenu = sousMenuTemp;
		}	
	}
}
