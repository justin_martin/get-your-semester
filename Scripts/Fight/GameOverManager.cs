﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {

	private int indexSelected;
	public GameObject[] sousMenu;
	[HideInInspector] public static bool gameOverActive;
	public GameObject Fade;
	public Sprite selectedButtonSprite, baseButtonSprite;
	public GameObject player;

	ButtonListener buttonListener;

	// Use this for initialization
	void Start () {
		buttonListener = FindObjectOfType<ButtonListener>();
	}
	
	// Update is called once per frame
	void Update () {
		if (gameOverActive && ((Input.GetKeyDown(KeyCode.DownArrow) || buttonListener.GetUpArrowClick()))) {
			setButtonState (sousMenu[indexSelected], false);
			if (indexSelected == sousMenu.GetLength (0) - 1) indexSelected = 0;
			else indexSelected++;
			setButtonState (sousMenu[indexSelected], true);
		}

		if (gameOverActive && (Input.GetKeyDown(KeyCode.UpArrow) || buttonListener.GetDownArrowClick())) {
			setButtonState (sousMenu[indexSelected], false);
			if (indexSelected == 0) indexSelected = sousMenu.GetLength (0) - 1;
			else indexSelected--;
			setButtonState (sousMenu[indexSelected], true);
		}

		if (gameOverActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) {
			if (sousMenu [indexSelected].name == "Continuer") {
				player.GetComponent<PlayerStats> ().health = 20;
				player.GetComponent<PlayerStats> ().isLive = true;
				StartCoroutine (Fade.GetComponent<Fade>().DoFadeWithPlayerMove (player.transform.position.x, player.transform.position.y, true));
				gameOverActive = false;
				gameObject.SetActive (false);
			}
			else if (sousMenu [indexSelected].name == "Sauvegarder") {
				FindObjectOfType<SystemCore> ().saveGame ();
			}
			else if (sousMenu [indexSelected].name == "Quitter") {
				gameOverActive = false;
				gameObject.SetActive (false);
				Initiate.Fade("GameMenu", Color.black, 2.0f);
			}
		}
	}

	public void gameOverState(bool state) {
		gameObject.SetActive (state);
		gameOverActive = state;
		cleanSousMenu ();
		indexSelected = 0;
		setButtonState(sousMenu[indexSelected], true); //Activation du bouton cancel par default
	}

	private void cleanSousMenu () {
		foreach (GameObject g in sousMenu)
			setButtonState (g, false);
	}


	void setButtonState (GameObject buttonSelected, bool state) {
		if (state)
			buttonSelected.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			buttonSelected.GetComponent<Image> ().sprite = baseButtonSprite;
	}
}
