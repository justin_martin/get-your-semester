﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnnemiBoxManager : MonoBehaviour {

	public GameObject avatarEnnemi;
	public Text nomEnnemi;
	public Text lvlEnnemi;

	public Text hpEnnemi;

	private Ennemi eStats;
	void Start () {}

	// Update is called once per frame
	void Update () {} 


	public void loadStats (GameObject ennemi) {
		ennemisBoxState (true);

		eStats = ennemi.GetComponent<Ennemi>();

		avatarEnnemi.GetComponent<Image> ().sprite = eStats.AvatarFace;
		nomEnnemi.text = eStats.pseudo;
		lvlEnnemi.text = " lvl : ????";

		hpEnnemi.text = eStats.health + " / " + eStats.MaxHealth;
		}

	public void ennemisBoxState(bool state) {
		gameObject.SetActive (state);
	}
}
