using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class ObjetsInfoManager : MonoBehaviour{

	public GameObject objetBox;
	public GameObject objetActionBox;

	public GameObject nomBox;
	public GameObject descriptionBox;
	private ButtonListener buttonListener;

	//public GameObject nbManaBox;

	[HideInInspector] public bool objetInfoActive; 

	public void objetInfoState (bool state) {
		Debug.Log (state);
		objetInfoActive = state; 
		gameObject.SetActive (state);
	}

	public void setObjet (Item itemSelected) {
		nomBox.GetComponent<Text> ().text = itemSelected.getNom();
		descriptionBox.GetComponent<Text> ().text = itemSelected.getDescription();
	//	nbManaBox.GetComponent<Text> ().text = "" + capacitySelected.manaCost;
	}

	void Start(){
		buttonListener = FindObjectOfType<ButtonListener> ();
	}

	void Update() {
		if (objetInfoActive && (Input.GetKeyDown (KeyCode.Escape) || buttonListener.GetButtonBClick())) {
			objetActionBox.GetComponent<ObjetsActionManager> ().objetActionActive = true;
			objetBox.GetComponent<ObjetsManagerF> ().objetState (true);
			objetBox.GetComponent<ObjetsManagerF> ().objetsFActive = false;
			objetInfoState (false);
		}
	}
}


