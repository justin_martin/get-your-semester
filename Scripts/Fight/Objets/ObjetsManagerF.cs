﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjetsManagerF : MonoBehaviour {

	public GameObject slider;
	public GameObject objetsActionBox;
	public GameObject actionBox;

	public GameObject templateObjet1;
	public GameObject templateObjet2;

	public Sprite objetsBaseSprite;
	public Sprite objetsSelectedSprite;

	public GameObject player;

	[HideInInspector] public bool objetsFActive;

	private PlayerStats pStats;
	private ItemMultiple[] itemTab;
	private ItemMultiple itemSelected;

	private int pointeurHaut;
	private int pointeurBas;
	private int nActive;
	private bool noObjet;

	private ButtonListener buttonListener;
	// Use this for initialization
	void Start () {
		buttonListener = FindObjectOfType<ButtonListener> ();
	}
	
	// Update is called once per frame
	void Update () { 

		if (!noObjet && pointeurHaut == 0)
			slider.GetComponent<SliderManager> ().UpState (false);
		else
			slider.GetComponent<SliderManager> ().UpState (true);
		
		if (!noObjet && pointeurBas == itemTab.GetLength (0) - 1)
			slider.GetComponent<SliderManager> ().DownState (false);
		else
			slider.GetComponent<SliderManager> ().DownState (true);

		if (!noObjet && objetsFActive && (Input.GetKeyDown (KeyCode.DownArrow) || buttonListener.GetDownArrowClick()) && nActive<itemTab.GetLength (0) - 1) {
			if (nActive == pointeurHaut) {
				setBoxActive (templateObjet1, false);
				nActive++;
				setBoxActive (templateObjet2, true);
			} 
			else if (nActive == pointeurBas) {
				setBoxActive (templateObjet2, false);
				nActive++;
				loadBox1 (itemTab[nActive]);
				if (nActive < itemTab.GetLength (0) - 1) {
					enableBox (templateObjet2);
					loadBox2 (itemTab [nActive + 1]);
					setBoxActive (templateObjet1, true);
					pointeurHaut = nActive;
					pointeurBas = nActive + 1;
				}
				else {
					setBoxActive (templateObjet1, true);
					pointeurHaut = nActive;
					pointeurBas = nActive;
					disableBox (templateObjet2);
				}
			}
			itemSelected = itemTab[nActive];
		}


		if (!noObjet && objetsFActive && (Input.GetKeyDown (KeyCode.UpArrow) || buttonListener.GetUpArrowClick()) && nActive>0) {
			if (nActive == pointeurHaut) {
				setBoxActive (templateObjet1, false);
				nActive--;
				enableBox (templateObjet2);
				loadBox2 (itemTab[nActive]);
				loadBox1 (itemTab[nActive-1]);
				setBoxActive (templateObjet2, true);
				pointeurHaut = nActive - 1;
				pointeurBas = nActive;
			}
			else if (nActive == pointeurBas) {
				setBoxActive (templateObjet2, false);
				nActive--;
				setBoxActive (templateObjet1, true);
			} 
			itemSelected = itemTab [nActive];
		}

		if (!noObjet && objetsFActive && (Input.GetKeyDown (KeyCode.Return) || buttonListener.GetButtonAClick())) {
			actionBox.SetActive (false);
			objetsFActive = false;
			objetsActionBox.GetComponent<ObjetsActionManager> ().objetActionState (true);
			objetsActionBox.GetComponent<ObjetsActionManager> ().setObjet(itemSelected.i);
		}

		if (objetsFActive && (Input.GetKeyDown (KeyCode.Escape) || buttonListener.GetButtonBClick())) {
			objetState (false);
			actionBox.GetComponent<ActionManager> ().actionActive = true;
		}
	}

	public void loadObjet(){
		pStats = player.GetComponent<PlayerStats> ();

		if (pStats.inventaire.Values.Count == 0) {
			noObjet = true;
			disableBox (templateObjet1);
			disableBox (templateObjet2);
			return;
		}
		noObjet = false;
		itemTab = new ItemMultiple[pStats.inventaire.Values.Count];

		int i = 0;
		foreach (KeyValuePair<Item, int> objet in pStats.inventaire) {
			itemTab [i] = new ItemMultiple (objet.Key, objet.Value);
			i++;
		}
		enableBox (templateObjet1);

		if (itemTab.GetLength (0) > 1) {
			enableBox (templateObjet2);

			pointeurBas = 1;
			pointeurHaut = 0;
			nActive = 0;

			loadBox1 (itemTab [0]); 
			loadBox2 (itemTab [1]);

			setBoxActive (templateObjet1, true);
			setBoxActive (templateObjet2, false);
		} 
		else {
			pointeurBas = 0;
			pointeurHaut = 0;
			nActive = 0;

			loadBox1 (itemTab [0]); 
			disableBox (templateObjet2);

			setBoxActive (templateObjet1, true);
		}
		itemSelected = itemTab [nActive];
	}

	void loadBox1 (ItemMultiple obj) {
		templateObjet1.transform.GetChild (0).GetComponent<Text> ().text = obj.i.getNom();
		templateObjet1.transform.GetChild (1).GetComponent<Text> ().text = "" + obj.nb;
	}

	void loadBox2 (ItemMultiple obj) {
		templateObjet2.transform.GetChild (0).GetComponent<Text> ().text = obj.i.getNom();
		templateObjet2.transform.GetChild (1).GetComponent<Text> ().text = "" + obj.nb;
	}

	void setBoxActive (GameObject templateCapacity, bool state) {
		if (state)
			templateCapacity.GetComponent<Image> ().sprite = objetsSelectedSprite;
		else
			templateCapacity.GetComponent<Image> ().sprite = objetsBaseSprite;
	}

	public void objetState(bool state) {
		gameObject.SetActive (state);
		objetsFActive = state;
	}

	void disableBox (GameObject templateObjet) {
		templateObjet.SetActive (false);
	}

	void enableBox (GameObject templateObjet) {
		templateObjet.SetActive (true);
	}
}
