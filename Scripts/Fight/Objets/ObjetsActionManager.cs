using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class ObjetsActionManager : MonoBehaviour {

	[HideInInspector] public bool objetActionActive;
	public GameObject[] sousMenu;

	public Sprite baseButtonSprite;
	public Sprite selectedButtonSprite;

	private int nActive;
	private Item objetSelected;
	public GameObject objetBox;
	public GameObject fightBox;
	public GameObject scoreBox;
	public GameObject objetInfoBox;
	public GameObject actionBox;

	private ButtonListener buttonListener;

	void Start() { 
		buttonListener = FindObjectOfType<ButtonListener> ();
	}

	void Update() {
		if (objetActionActive && (Input.GetKeyDown (KeyCode.DownArrow) || buttonListener.GetDownArrowClick())) {
			setButtonState (sousMenu [nActive], false);
			if (nActive == sousMenu.GetLength (0) - 1)
				nActive = 0;
			else
				nActive++;
			setButtonState (sousMenu [nActive], true);
		}

		if (objetActionActive && (Input.GetKeyDown (KeyCode.UpArrow) || buttonListener.GetUpArrowClick())) {
			setButtonState (sousMenu [nActive], false);
			if (nActive == 0)
				nActive = sousMenu.GetLength (0) - 1;
			else
				nActive--;
			setButtonState (sousMenu [nActive], true);
		}

		if (objetActionActive && (Input.GetKeyDown (KeyCode.Return)) || buttonListener.GetButtonAClick()) {
			if (sousMenu [nActive].name == "Utiliser") {
				objetActionActive = false;
				objetBox.SetActive (false);
				objetInfoBox.GetComponent<ObjetsInfoManager> ().objetInfoState (false);
				scoreBox.GetComponent<ScoreManager> ().scoreState (true);
				fightBox.GetComponent<FightManager> ().useObjet (objetSelected);
			} else if (sousMenu [nActive].name == "Info") {
				objetActionActive = false;
				objetBox.SetActive (false);
				objetInfoBox.GetComponent<ObjetsInfoManager> ().objetInfoState (true);
				objetInfoBox.GetComponent<ObjetsInfoManager> ().setObjet (objetSelected);
			} 
		}

		if (objetActionActive && (Input.GetKeyDown (KeyCode.Escape)) || buttonListener.GetButtonBClick()) {
			objetActionActive = false;
			gameObject.SetActive (false);

			actionBox.GetComponent<ActionManager> ().actionActive = false;
			actionBox.SetActive (true);

			objetBox.GetComponent<ObjetsManagerF> ().objetsFActive = true;
			objetBox.SetActive (true);
		}
	}

	public void objetActionState (bool state) {
		objetActionActive = state;
		gameObject.SetActive (state);
		nActive = 0;
		cleanButtonState ();
		setButtonState (sousMenu [nActive], true);
	}

	public void setObjet(Item objetSelectedArg) {
		objetSelected = objetSelectedArg;
	}

	public void cleanButtonState(){
		foreach (GameObject g in sousMenu) {
			setButtonState (g, false);
		}
	}

	void setButtonState (GameObject buttonSelected, bool state) {
		if (state)
			buttonSelected.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			buttonSelected.GetComponent<Image> ().sprite = baseButtonSprite;
	}
}

