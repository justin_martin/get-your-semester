﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FightManager : MonoBehaviour {

	private float xPlayerCoord, yPlayerCoord;
	private float xEnnemiCoord, yEnnemiCoord, zEnnemiCoord;

	private float x_input, y_input;
	private Sprite ennemiPosition;

	[SerializeField] private AudioClip fightSound;

	[HideInInspector] public static bool fightActive;

	[SerializeField] private GameObject player;
	[HideInInspector] public GameObject ennemi;

	[SerializeField] private GameObject playerBox;
	[SerializeField] private GameObject ennemiBox;

	[SerializeField] private GameObject dialogueEndBox;

	[SerializeField] private GameObject questionFightBox;
	[SerializeField] private GameObject responseBox;
	[SerializeField] private GameObject actionBox;
	[SerializeField] private GameObject scoreBox;
	[SerializeField] private GameObject Fade;
	[SerializeField] private GameObject fightBox;
	[SerializeField] private GameObject spaceToContinue;
	private PlayerStats pStats;
	private Ennemi eStats;

	private System.Random rnd;
	private int indexQuestion;

	private bool scoreDisplay;

	private bool winDialogueActive;

	private bool winQuestion;

	private ButtonListener buttonListener;

	public void StateFight(bool state){

		winQuestion = false;
		winDialogueActive = false;

		SoundManager soundMan = FindObjectOfType<SoundManager> ();
		soundMan.playSoundGlobal (fightSound);
		/** Save Data **/
		xPlayerCoord = player.transform.position.x;
		yPlayerCoord = player.transform.position.y;

		xEnnemiCoord = ennemi.transform.position.x;
		yEnnemiCoord = ennemi.transform.position.y;
		zEnnemiCoord = ennemi.transform.position.z;


		x_input = player.GetComponent<Animator> ().GetFloat ("input_x");
		y_input = player.GetComponent<Animator> ().GetFloat ("input_y");

		ennemiPosition = ennemi.GetComponent<SpriteRenderer> ().sprite;
		/* ====================== **/

		pStats = player.GetComponent<PlayerStats>();
		eStats = ennemi.GetComponent<Ennemi> ();
		winDialogueActive = false;
		fightActive = state;
		gameObject.SetActive(state);
		fightBox.SetActive (true);

		actionBox.SetActive (true);
		actionBox.GetComponent<ActionManager> ().actionState (true);
		actionBox.GetComponent<ActionManager> ().setEnnemi(ennemi);

		scoreBox.SetActive (true);
		scoreBox.GetComponent<ScoreManager> ().scoreState (true);
		scoreBox.GetComponent<ScoreManager> ().setText (pStats.pseudo + " affronte " + eStats.pseudo);

		player.transform.position = new Vector3 ((float)-8.74, (float)-3.73, (float)-0.2);
		ennemi.transform.position = new Vector3 ((float)-7.43, (float)-3.73, (float)-0.2);


		player.GetComponent<PlayerMovement> ().anim.SetFloat ("input_x", 1);
		player.GetComponent<PlayerMovement> ().anim.SetFloat ("input_y", 0);

		ennemi.GetComponent<SpriteRenderer> ().sprite = eStats.leftFace;	
		ennemi.GetComponent<Ennemi> ().initQuestion ();

		playerBox.SetActive (true);
		ennemiBox.SetActive (false);
		playerBox.GetComponent<PlayerBoxManager> ().loadStats (player);
		ennemiBox.GetComponent<EnnemiBoxManager> ().loadStats (ennemi);
	}

	// Use this for initialization
	void Start () {
		buttonListener = FindObjectOfType<ButtonListener> ();
		rnd = new System.Random ();	
	}
	
	// Update is called once per frame
	void Update () {
		if (fightActive && !winDialogueActive) {
			if (!player.GetComponent<PlayerStats> ().isLive)
				LoseDialogue ();
			if (!ennemi.GetComponent<Ennemi> ().isLive)
				WinDialogue ();
		}

		if (scoreDisplay && (Input.GetKeyDown (KeyCode.Return) || buttonListener.GetButtonAClick())) {
			scoreDisplay = false;
			scoreBox.GetComponent<ScoreManager> ().scoreState (false);
			responseBox.SetActive (true);
			responseBox.GetComponent<ResponseManager> ().responseState (true);
			questionFightBox.SetActive (true);
			spaceToContinue.SetActive (false);

			if (winQuestion)
				nextQuestion ();
		}
	}
		
	public void useObjet (Item objetSelected) {

		pStats.useItem (objetSelected);

		String rFight = pStats.pseudo + " utilise " + objetSelected.getNom ();

		playerBox.GetComponent<PlayerBoxManager> ().loadStats (player);
		ennemiBox.GetComponent<EnnemiBoxManager> ().loadStats (ennemi);

		scoreBox.GetComponent<ScoreManager> ().setText (rFight);
		//StartCoroutine (WaitText1 ());

	}		

	/*private IEnumerator WaitText1() {
		while (scoreBox.GetComponent<ScoreManager> ().loadingText)
			yield return new WaitForSeconds (0.1f);
			
		yield return new WaitForSeconds (1f);

		if (!eStats.isLive)
			WinDialogue ();
		else IAFight ();

	}*/

	private IEnumerator WaitText2() {
		while (scoreBox.GetComponent<ScoreManager> ().loadingText)
			yield return new WaitForSeconds (0.1f);

		actionBox.GetComponent<ActionManager> ().actionState (true);

		if (!pStats.isLive)
			LoseDialogue ();
	}

	/*** END FIGHT ****/
	void WinDialogue () {
		winDialogueActive = true;
		//pStats.addCapacity (eStats.cap);
		TextAsset winFight = eStats.winFight;
		if (!pStats.checkGemme (eStats.gemme)) pStats.addGemme (eStats.gemme);
		else winFight = eStats.alreadyWin;

		eStats.health = eStats.MaxHealth;
		eStats.extractQuestions ();

		if (pStats.hadAllGemme()) dialogueEndBox.GetComponent<DialogueHolder> ().winGame = true;
		else dialogueEndBox.GetComponent<DialogueHolder> ().winGame = false;
		
		questionFightBox.SetActive (false);
		responseBox.SetActive (false);

		dialogueEndBox.GetComponent<DialogueHolder> ().dialogueText = winFight;
		dialogueEndBox.GetComponent<DialogueHolder> ().gameOverDialogue = false;
		TpBack ();
		//give gemme, money
		StartCoroutine(WaitFade());
	}

	void LoseDialogue () {
		dialogueEndBox.GetComponent<DialogueHolder> ().dialogueText = eStats.loseFight;
		dialogueEndBox.GetComponent<DialogueHolder> ().winGame = false;
		dialogueEndBox.GetComponent<DialogueHolder> ().gameOverDialogue = true;

		eStats.extractQuestions ();
		questionFightBox.SetActive (false);
		responseBox.SetActive (false);
		TpBack ();
		//keep money and time

		StartCoroutine (WaitFade());
	}

	void TpBack() {
		ennemi.transform.position = new Vector3 (xEnnemiCoord, yEnnemiCoord, zEnnemiCoord);
		ennemi.GetComponent<SpriteRenderer> ().sprite = ennemiPosition;

		StartCoroutine(Fade.GetComponent<Fade> ().DoFadeWithPlayerMove (xPlayerCoord, yPlayerCoord, true));
		player.GetComponent<Animator> ().SetFloat ("input_x", x_input);
		player.GetComponent<Animator> ().SetFloat ("input_y", y_input);
	}

	private IEnumerator WaitFade() {
		while (Fade.GetComponent<Fade> ().fadeActive)
			yield return new WaitForSeconds (0.1f);

		dialogueEndBox.GetComponent<DialogueHolder> ().activeDialog();
		fightActive = false;
		gameObject.SetActive (false);
	}

	public void setEnnemi(GameObject ennemi){
		this.ennemi = ennemi;
	}

	public void loseHpProf () {
		responseBox.GetComponent<ResponseManager> ().responseState (false);
		questionFightBox.SetActive (false);
		eStats.sufferDamage (5);
		ennemiBox.GetComponent<EnnemiBoxManager>().loadStats(ennemi);
		playerBox.GetComponent<PlayerBoxManager> ().loadStats (player);

		scoreBox.SetActive (true);
		scoreBox.GetComponent<ScoreManager> ().scoreState (true);
		scoreBox.GetComponent<ScoreManager> ().setText ("Bien joué, continue comme ça\n" + eStats.pseudo + " perd 5 points de vie");
		spaceToContinue.SetActive (true);

		scoreDisplay = true;
		winQuestion = true;
	}

	public void loseHpPlayer ()	{
		responseBox.GetComponent<ResponseManager> ().responseState (false);
		questionFightBox.SetActive (false);
		pStats.sufferDamage (5);
		playerBox.GetComponent<PlayerBoxManager> ().loadStats (player);
		ennemiBox.GetComponent<EnnemiBoxManager>().loadStats(ennemi);

		scoreBox.SetActive (true);
		scoreBox.GetComponent<ScoreManager> ().scoreState (true);
		scoreBox.GetComponent<ScoreManager> ().setText ("Faux, aller tu peux le faire\n" + pStats.pseudo + " perd 5 points de vie");
		spaceToContinue.SetActive (true);

		scoreDisplay = true;
		winQuestion = false;
	}

	public void nextQuestion(){
		if (eStats.isLive) {
			if (indexQuestion == eStats.questionsList.Count) {
				LoseDialogue ();
			} else {
				questionFightBox.SetActive (true);
				questionFightBox.GetComponent<QuestionBoxManager> ().showQuestion (eStats.getQuestion ());
				responseBox.SetActive (true);
				responseBox.GetComponent<ResponseManager> ().responseState (true);
			}

		} else {
			LoseDialogue ();
		}
	}
}