﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionManager : MonoBehaviour {

	public GameObject[] sousMenu;

	public Sprite baseButtonSprite;
	public Sprite selectedButtonSprite;

	private int indexSelected;
	private GameObject ennemi;

	[HideInInspector] public bool actionActive;
	public GameObject responseBox;
	public GameObject scoreBox;
	public GameObject objetBox;
	public GameObject questionBox;
	public GameObject slider;

	private Ennemi eStats; 
	ButtonListener buttonListener;

	// Use this for initialization
	void Start () {	
	}
	
	// Update is called once per frame
	void Update () { 
		if (actionActive && (Input.GetKeyDown(KeyCode.DownArrow) || buttonListener.GetDownArrowClick())) {
			setButtonState (sousMenu [indexSelected], false);
			if (indexSelected == sousMenu.GetLength (0) - 1)
				indexSelected = 0;
			else
				indexSelected++;
			setButtonState (sousMenu [indexSelected], true);
		}

		if (actionActive && (Input.GetKeyDown(KeyCode.UpArrow) || buttonListener.GetUpArrowClick())) {
			setButtonState (sousMenu [indexSelected], false);
			if (indexSelected == 0)
				indexSelected = sousMenu.GetLength (0) - 1;
			else
				indexSelected--;
			setButtonState (sousMenu [indexSelected], true);
		}

		if (actionActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) {
	
			if(sousMenu[indexSelected].name == "Attaquer"){
				actionActive = false;
				scoreBox.GetComponent<ScoreManager> ().scoreState (false);
				FindObjectOfType<FightManager> ().nextQuestion ();
			} else if (sousMenu [indexSelected].name == "Objets") {
				actionActive = false;
				scoreBox.GetComponent<ScoreManager> ().scoreState (false);
				objetBox.GetComponent<ObjetsManagerF> ().objetState (true);
				objetBox.GetComponent<ObjetsManagerF> ().loadObjet ();
			}
			else if(sousMenu [indexSelected].name == "Fuite") 
				tryToFuite();
		}
	}


	public void actionState(bool state) {
		
		buttonListener = FindObjectOfType<ButtonListener> ();
		gameObject.SetActive (state);
		actionActive = state;
		indexSelected = 0;
		cleanButtonState ();
		setButtonState (sousMenu [indexSelected], true);
	}

	public void cleanButtonState(){
		foreach (GameObject g in sousMenu) {
			setButtonState (g, false);
		}
	}

	void setButtonState (GameObject buttonSelected, bool state) {
		if (state)
			buttonSelected.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			buttonSelected.GetComponent<Image> ().sprite = baseButtonSprite;
	}

	void tryToFuite(){
		
	}

	public void setEnnemi (GameObject ennemi) {
		this.ennemi = ennemi;
		eStats = ennemi.GetComponent<Ennemi> ();
	}
}