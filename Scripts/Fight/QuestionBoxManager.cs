﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionBoxManager : MonoBehaviour {
	// Va tout afficher dans le panel etc
	private Dictionary<string,string> affichQuestions;
	private Question currentQuestion;
	[SerializeField] private Text[] answerTab;
	[SerializeField] private Text question;
	[SerializeField] private GameObject panel;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void showQuestion(Question q){
		affichQuestions = new Dictionary<string,string> ();
		panel.SetActive (true);
		this.currentQuestion = q;
		// quelque chose avec checkbox
		affichQuestions.Add("A",q.allAnswers[0]);
		affichQuestions.Add("B",q.allAnswers[1]);
		affichQuestions.Add("C",q.allAnswers[2]);
		affichQuestions.Add("D",q.allAnswers[3]);

		int index = 0;
		foreach (Text t in answerTab) {
			t.text = q.allAnswers [index];
			index++;
		}

		question.text = q.question;
	}

	public bool checkAnswer(string buttonClicked){
		string answer;
		affichQuestions.TryGetValue (buttonClicked,out answer);
		return (currentQuestion.goodAns == answer);
	}
}
