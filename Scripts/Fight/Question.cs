﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Question {
	// chaîne de caractères qui va contenir la question
	public string question;
	// chaîne de caractères qui va contenir la bonne réponse
	public string goodAns;
	// tableau de chaînes de caractères qui va contenir toutes les réponses, y compris la bonne réponse
	public string[] allAnswers;

	private static Random rnd;

	public Question (string question, string goodAns, string badAns1, string badAns2, string badAns3)
	{
		this.question = question;
		this.goodAns = goodAns;
		this.allAnswers = new string[4] {goodAns,badAns1,badAns2,badAns3};
		shuffle (allAnswers);
	}
		
	bool isGoodAnswer(string answer){
		if (answer == goodAns)
			return true;
		else
			return false;
	}

	void shuffle(string[] rep){
		for (int i = 0; i < rep.Length; i++) {
			string tmp = rep [i];
			int r = Random.Range (i, rep.Length);
			rep [i] = rep [r];
			rep [r] = tmp;
		}
	}
}