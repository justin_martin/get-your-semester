using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class CapacityInfoManager : MonoBehaviour{

	public GameObject capacityBox;
	public GameObject capacityActionBox;

	public GameObject nomBox;
	public GameObject descriptionBox;
	public GameObject nbManaBox;

	[HideInInspector] public bool capacityInfoActive; 

	private ButtonListener buttonListener;
	// Use this for initialization
	void Start () {
		buttonListener = FindObjectOfType<ButtonListener> ();
	}

	public void capacityInfoSate (bool state) {
		capacityInfoActive = state; 
		gameObject.SetActive (state);
	}

	public void setCapacity (Capacity capacitySelected) {
		nomBox.GetComponent<Text> ().text = capacitySelected.nom;
		descriptionBox.GetComponent<Text> ().text = capacitySelected.description;
		nbManaBox.GetComponent<Text> ().text = "" + capacitySelected.manaCost;
	}

	void Update() {
		if (capacityInfoActive && Input.GetKeyDown (KeyCode.Escape) || buttonListener.GetButtonBClick()) {
			capacityActionBox.GetComponent<CapacityActionManager> ().capacityActionActive = true;
//			capacityBox.GetComponent<CapacityManager> ().capacityState (true);
			capacityBox.GetComponent<CapacityManager> ().capacityActive = false;
			capacityInfoSate (false);
		}
	}
}


