﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CapacityManager : MonoBehaviour {
	
	public GameObject slider;
	public GameObject capacityactionBox;
	public GameObject actionBox;

	public GameObject templateCapacity1;
	public GameObject templateCapacity2;

	public Sprite capacityBaseSprite;
	public Sprite capacitySelectedSprite;

	public GameObject player;

	[HideInInspector] public bool capacityActive;

	private PlayerStats pStats;
	private Capacity[] capacityTab;
	private Capacity capacitySelected;

	private int pointeurHaut;
	private int pointeurBas;
	private int nActive;
	private bool noCapacity;

	private ButtonListener buttonListener;
	/*
	// Use this for initialization
	void Start () {
		buttonListener = FindObjectOfType<ButtonListener> ();
	}
	
	// Update is called once per frame
	void Update () { 

		if (!noCapacity && pointeurHaut == 0)
			slider.GetComponent<SliderManager> ().UpState (false);
		else
			slider.GetComponent<SliderManager> ().UpState (true);
		
		if (!noCapacity && pointeurBas == capacityTab.GetLength (0) - 1)
			slider.GetComponent<SliderManager> ().DownState (false);
		else
			slider.GetComponent<SliderManager> ().DownState (true);

		if (!noCapacity && capacityActive && (Input.GetKeyDown (KeyCode.DownArrow) || buttonListener.GetDownArrowClick()) && nActive<capacityTab.GetLength (0) - 1) {
			if (nActive == pointeurHaut) {
				setBoxActive (templateCapacity1, false);
				nActive++;
				setBoxActive (templateCapacity2, true);
			} 
			else if (nActive == pointeurBas) {
				setBoxActive (templateCapacity2, false);
				nActive++;
				loadBox1 (capacityTab[nActive]);
				if (nActive < capacityTab.GetLength (0) - 1) {
					enableBox (templateCapacity2);
					loadBox2 (capacityTab [nActive + 1]);
					setBoxActive (templateCapacity1, true);
					pointeurHaut = nActive;
					pointeurBas = nActive + 1;
				}
				else {
					setBoxActive (templateCapacity1, true);
					pointeurHaut = nActive;
					pointeurBas = nActive;
					disableBox (templateCapacity2);
				}
			}
			capacitySelected = capacityTab [nActive];
		}

		/*
		if (!noCapacity && capacityActive && (Input.GetKeyDown (KeyCode.UpArrow) || buttonListener.GetUpArrowClick()) && nActive>0) {
			if (nActive == pointeurHaut) {
				setBoxActive (templateCapacity1, false);
				nActive--;
				enableBox (templateCapacity2);
				loadBox2 (capacityTab[nActive]);
				loadBox1 (capacityTab[nActive-1]);
				setBoxActive (templateCapacity2, true);
				pointeurHaut = nActive - 1;
				pointeurBas = nActive;
			}
			else if (nActive == pointeurBas) {
				setBoxActive (templateCapacity2, false);
				nActive--;
				setBoxActive (templateCapacity1, true);
			}
			capacitySelected = capacityTab [nActive];
		}

		if (!noCapacity && capacityActive && (Input.GetKeyDown (KeyCode.Return) || buttonListener.GetButtonAClick())){
			if (capacitySelected.manaCost <= player.GetComponent<PlayerStats> ().mana) {
				actionBox.SetActive (false);
				capacityActive = false;
				capacityactionBox.GetComponent<CapacityActionManager> ().capacityActionState (true);
				capacityactionBox.GetComponent<CapacityActionManager> ().setCapacity (capacitySelected);
			}
		}

		if (capacityActive && (Input.GetKeyDown (KeyCode.Escape) || buttonListener.GetButtonBClick())) {
			capacityState (false);
			actionBox.GetComponent<ActionManager> ().actionActive = true;
		}
	}

	public void loadCapacity(){
		pStats = player.GetComponent<PlayerStats> ();

		if(pStats.capacityList.Count == 0){
			disableBox (templateCapacity1);
			disableBox (templateCapacity2);
			noCapacity = true;
			return;
		}

		capacityTab = new Capacity[pStats.capacityList.Count];
		pStats.capacityList.CopyTo (capacityTab);

		noCapacity = false;
		enableBox (templateCapacity1);

		if (capacityTab.GetLength (0) > 1) {
			enableBox (templateCapacity2);

			pointeurBas = 1;
			pointeurHaut = 0;
			nActive = 0;

			loadBox1 (capacityTab [0]); 
			loadBox2 (capacityTab [1]);

			setBoxActive (templateCapacity1, true);
			setBoxActive (templateCapacity2, false);
		} else {
			pointeurBas = 0;
			pointeurHaut = 0;
			nActive = 0;

			loadBox1 (capacityTab [0]); 
			disableBox (templateCapacity2);

			setBoxActive (templateCapacity1, true);
		}
		capacitySelected = capacityTab [nActive];
	}

	void loadBox1 (Capacity cap) {
		if (cap.manaCost > player.GetComponent<PlayerStats> ().mana) {
			templateCapacity1.transform.GetChild (0).GetComponent<Text> ().color = Color.red;
			templateCapacity1.transform.GetChild (1).GetComponent<Text> ().color = Color.red;
		} 
		else {
			templateCapacity1.transform.GetChild (0).GetComponent<Text> ().color = Color.white;
			templateCapacity1.transform.GetChild (1).GetComponent<Text> ().color = Color.white;
		}
		templateCapacity1.transform.GetChild (0).GetComponent<Text> ().text = cap.nom;
		templateCapacity1.transform.GetChild (1).GetComponent<Text> ().text = "" + cap.manaCost;
	}

	void loadBox2 (Capacity cap) {
		if (cap.manaCost > player.GetComponent<PlayerStats> ().mana) {
			templateCapacity2.transform.GetChild (0).GetComponent<Text> ().color = Color.red;
			templateCapacity2.transform.GetChild (1).GetComponent<Text> ().color = Color.red;
		} 
		else {
			templateCapacity2.transform.GetChild (0).GetComponent<Text> ().color = Color.white;
			templateCapacity2.transform.GetChild (1).GetComponent<Text> ().color = Color.white;
		}
		templateCapacity2.transform.GetChild (0).GetComponent<Text> ().text = cap.nom;
		templateCapacity2.transform.GetChild (1).GetComponent<Text> ().text = "" + cap.manaCost;
	}

	void setBoxActive (GameObject templateCapacity, bool state) {
		if (state)
			templateCapacity.GetComponent<Image> ().sprite = capacitySelectedSprite;
		else
			templateCapacity.GetComponent<Image> ().sprite = capacityBaseSprite;
	}

	public void capacityState(bool state) {
		gameObject.SetActive (state);
		capacityActive = state;
	}

	void disableBox (GameObject templateCapacity) {
		templateCapacity.SetActive (false);
	}

	void enableBox (GameObject templateCapacity) {
		templateCapacity.SetActive (true);
	}
	*/
}
