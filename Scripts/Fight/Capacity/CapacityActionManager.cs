using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class CapacityActionManager : MonoBehaviour {

	[HideInInspector] public bool capacityActionActive;
	public GameObject[] sousMenu;

	public Sprite baseButtonSprite;
	public Sprite selectedButtonSprite;

	private int nActive;
	private Capacity capacitySelected;
	public GameObject capacityBox;
	public GameObject fightBox;
	public GameObject scoreBox;
	public GameObject capacityInfoBox;
	public GameObject actionBox;
	private ButtonListener buttonListener;
	// Use this for initialization
	void Start () {
		buttonListener = FindObjectOfType<ButtonListener> ();
	}


	void Update() {
		if (capacityActionActive && (Input.GetKeyDown (KeyCode.DownArrow) || buttonListener.GetDownArrowClick())) {
			setButtonState (sousMenu [nActive], false);
			if (nActive == sousMenu.GetLength (0) - 1)
				nActive = 0;
			else
				nActive++;
			setButtonState (sousMenu [nActive], true);
		}

		if (capacityActionActive && (Input.GetKeyDown (KeyCode.UpArrow) || buttonListener.GetUpArrowClick())) {
			setButtonState (sousMenu [nActive], false);
			if (nActive == 0)
				nActive = sousMenu.GetLength (0) - 1;
			else
				nActive--;
			setButtonState (sousMenu [nActive], true);
		}

		if (capacityActionActive && (Input.GetKeyDown (KeyCode.Escape) || buttonListener.GetButtonBClick())) {
			capacityActionActive = false;
			gameObject.SetActive (false);

			actionBox.GetComponent<ActionManager> ().actionActive = false;
			actionBox.SetActive (true);

			capacityBox.GetComponent<CapacityManager> ().capacityActive = true;
		}
	}

	public void capacityActionState (bool state) {
		capacityActionActive = state;
		gameObject.SetActive (state);
		nActive = 0;
		cleanButtonState ();
		setButtonState (sousMenu [nActive], true);
	}

	public void setCapacity (Capacity capacitySelectedArg) {
		capacitySelected = capacitySelectedArg;
	}

	public void cleanButtonState(){
		foreach (GameObject g in sousMenu) {
			setButtonState (g, false);
		}
	}

	void setButtonState (GameObject buttonSelected, bool state) {
		if (state)
			buttonSelected.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			buttonSelected.GetComponent<Image> ().sprite = baseButtonSprite;
	}
}

