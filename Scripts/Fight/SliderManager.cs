﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderManager : MonoBehaviour {

	public GameObject UpBox;
	public GameObject DownBox;

	public Sprite UpEnableSprite;
	public Sprite DownEnableSprite;

	public Sprite UpDisableSprite;
	public Sprite DownDisableSprite;
	// Use this for initialization
	void Start () {	}
	
	// Update is called once per frame
	void Update () { }


	public void UpState(bool state) {
		if (state)
			UpBox.GetComponent<Image> ().sprite = UpEnableSprite;
		else
			UpBox.GetComponent<Image> ().sprite = UpDisableSprite;
	}

	public void DownState(bool state) {
		if (state)
			DownBox.GetComponent<Image> ().sprite = DownEnableSprite;
		else
			DownBox.GetComponent<Image> ().sprite = DownDisableSprite;
	}
}
