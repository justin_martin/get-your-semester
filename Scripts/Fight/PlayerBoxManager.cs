﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBoxManager : MonoBehaviour {


	public GameObject avatarPlayer;
	public Text nomPlayer;
	public Text lvlPlayer;

	public Text hpPlayer;

	private PlayerStats pStats;

	void Start () {}

	// Update is called once per frame
	void Update () {}

	public void loadStats (GameObject player) {

		pStats = FindObjectOfType<PlayerStats> ();

		avatarPlayer.GetComponent<Image> ().sprite = pStats.AvatarFace;
		nomPlayer.text = pStats.pseudo;
		lvlPlayer.text = " lvl : " + pStats.lvl;

		hpPlayer.text = pStats.health + " / " + pStats.healthMax;
	}

	public void animLoseHp(){
	
	}
}
