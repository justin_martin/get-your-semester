using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

	[HideInInspector] public bool loadingText;

	private float waitTime = 0.01f;

	[HideInInspector] bool scoreActive;
	[SerializeField] private Text scoreText; 

	public void scoreState (bool state) {
		gameObject.SetActive (state);
		scoreActive = state;
	}

	public void setText (string text) {
		StartCoroutine (LoadLetters (text));

	}

	private IEnumerator LoadLetters(string completeText) {
		loadingText = true;
	
		scoreText.text = "";

		int textSize = 0;
		while (textSize < completeText.Length) {
			scoreText.text += completeText[textSize++];
			yield return new WaitForSeconds(waitTime);
		}

		loadingText = false;
	}
}

