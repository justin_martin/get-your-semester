﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class ObjetsManager : MonoBehaviour {

	//To load Stats
	public GameObject player;

	//To Genarate Objets
	public int ySpace;
	public int yCoord;

	public int yStatic; 

	public int xCoord;
	public GameObject templateObjet;
	public GameObject contentScroll;
	public GameObject slider;

	//To change set background of selected objet
	public Sprite objetActiveSprite;
	public Sprite objetBaseSprite;

	//To see if menu is active
	private bool objetMenuActive;

	//To strorage stats
	private PlayerStats pStats;

	//To storage all objets
	private GameObject[] objetsGameComponent;
	private Item[] items;
	private int[] quantity;

	private int indexSelected = -1;
	private bool objetActive=true;

	private bool noObjet;
	private int pointeurHaut;
	private int pointeurBas;

	public Item itemSelected;

	ButtonListener buttonListener;

	public GameObject ObjetActiveBox;
	// Use this for initialization
	void Start () {	
		buttonListener = FindObjectOfType<ButtonListener> ();
	}

	// Update is called once per frame
	void Update () {

		if (!noObjet && pointeurHaut == 0)
			slider.GetComponent<SliderManager> ().UpState (false);
		else
			slider.GetComponent<SliderManager> ().UpState (true);
		
		if (!noObjet && pointeurBas == items.GetLength (0) - 1)
			slider.GetComponent<SliderManager> ().DownState (false);
		else
			slider.GetComponent<SliderManager> ().DownState (true);
		
		if (indexSelected != -1 && objetActive && (Input.GetKeyDown(KeyCode.DownArrow) || buttonListener.GetDownArrowClick()) && indexSelected<items.GetLength (0) - 1) {
			if (indexSelected == pointeurHaut) {
				setActiveObjet (indexSelected, false);
				indexSelected++;
				setActiveObjet (indexSelected, true);
			} 
			else if (indexSelected == pointeurBas) {

				pointeurHaut = indexSelected;
				indexSelected++;
				pointeurBas = indexSelected;

				loadItemsBack ();
			}
		}

		if (indexSelected != -1 && objetActive && (Input.GetKeyDown(KeyCode.UpArrow) || buttonListener.GetUpArrowClick())){
			setActiveObjet (indexSelected, false);
			if (indexSelected == 0)
				indexSelected = items.GetLength (0) - 1;
			else
				indexSelected--;
			
			setActiveObjet (indexSelected, true);
		}

		if (indexSelected != -1 && objetActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) {
			objetActive = false;
			ObjetActiveBox.GetComponent<ObjetsActiveManager> ().StateObjetActive (true, items[indexSelected]);
		}

		if(objetActive && (Input.GetKeyDown(KeyCode.Escape) || buttonListener.GetButtonBClick())){
			StateObjets(false);
		}
	}


	public void StateObjets(bool state){
		objetMenuActive = state;
		gameObject.SetActive(state);
		objetActive = state;
		LoadItem();
	}

	public void LoadItem() {

		cleanItem ();
		pStats = FindObjectOfType<PlayerStats>();

		if (pStats.inventaire.Count <= 0) {
			noObjet = true;
			return;
		}
			
		noObjet = false;
		
		objetsGameComponent = new GameObject[4];

		quantity = new int[pStats.inventaire.Values.Count];  //Séparation du dictionnaire
		items = new Item[pStats.inventaire.Values.Count];

		int nb = 0;

		foreach(KeyValuePair<Item, int> objet in pStats.inventaire) {
			indexSelected++;
			items[indexSelected] = objet.Key; //stockage dans tableur de tous les objets
			quantity[indexSelected] = objet.Value;

			if ((nb++) >= 4) //Max 4 éléments sur l'interfaces
				break;
			
			GameObject clone = Instantiate (templateObjet, contentScroll.transform); //Génération des objets
			clone.transform.position = new Vector3 (xCoord, yCoord);
		
			setObjetStats (clone, items[indexSelected], quantity[indexSelected]);	//On set les données
	
			objetsGameComponent[indexSelected] = clone;	//stockage dans tableau des cases objets 0 à 3 (4 cases dispo)
			yCoord -= ySpace;
		}


		if(objetsGameComponent[0] != null) setActiveObjet(0, true);
	}

	void cleanItem () {
		yCoord = yStatic;
		items = null;
		objetsGameComponent = null;
		pStats = null;
		indexSelected = -1;

		setIsActive (true);
		foreach (Transform child in contentScroll.transform) {
			Destroy (child.gameObject);
		}
	}

	void setObjetStats (GameObject clone, Item itemArg, int quantityArg) {
		clone.transform.GetChild (0).gameObject.name = "Nom" + itemArg.getNom ();
		clone.transform.GetChild (0).gameObject.GetComponent<Text>().text = itemArg.getNom ();

		clone.transform.GetChild (1).gameObject.name = "Nb" + itemArg.getNom ();
		clone.transform.GetChild (1).gameObject.GetComponent<Text>().text = "x" + quantityArg;
	}

	void setActiveObjet (int indexClone, bool isActive)	{
		if (isActive) {
			//change background active objet
			objetsGameComponent[indexClone].GetComponent<Image> ().sprite = objetActiveSprite;
			//change name and description

			gameObject.transform.GetChild (2).gameObject.transform.GetChild (0).GetComponent<Text> ().text = "" + items[indexClone].getNom();
			gameObject.transform.GetChild (2).gameObject.transform.GetChild (1).GetComponent<Text> ().text = "" + items[indexClone].getDescription ();

			indexSelected = indexClone;
		} else {
			objetsGameComponent[indexClone].GetComponent<Image> ().sprite = objetBaseSprite;
		}
	}

	void loadItemsBack () {
		int cpt = 4;
		for (int i = 0; i < 4; i++) {
			setObjetStats (contentScroll.transform.GetChild (i).gameObject, items [indexSelected - cpt], quantity [indexSelected - cpt]);
			cpt--;
		}
	}
		
	public void setIsActive(bool state){
		objetActive = state;
	}
}
