﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjetsActiveManager : MonoBehaviour {

	//To Load Stats
	public GameObject player;

	//To see if menu is active
	private bool objetChoiceActive;

	//To delete item
	private PlayerStats pStats;
	private int nActive;

	public Sprite baseButtonSprite;
	public Sprite selectedButtonSprite;

	public Item itemToUse;

	public Text nomObjet;

	public Text nbHeart;

	public GameObject[] button;
	public GameObject objetBox;

	ButtonListener buttonListener;

	// Use this for initialization
	void Start () {
		buttonListener = FindObjectOfType<ButtonListener>();	
	}
			
	// Update is called once per frame
	void Update () {
		
		if(objetChoiceActive && (Input.GetKeyDown(KeyCode.DownArrow) || buttonListener.GetDownArrowClick())){
			setButtonState (button[nActive], false);
			if (nActive == button.GetLength (0) - 1) 
				nActive = 0;
			else
				nActive++;
			setButtonState (button[nActive], true);
		}

		if(objetChoiceActive && (Input.GetKeyDown(KeyCode.UpArrow) || buttonListener.GetUpArrowClick())){
			setButtonState (button[nActive], false);
			if (nActive == 0) 
				nActive = button.GetLength(0)-1;
			else 
				nActive--;
			setButtonState (button[nActive], true);
		}

		if (objetChoiceActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) {
			itemActive(button[nActive], itemToUse);
			gameObject.SetActive (false);
			objetBox.GetComponent<ObjetsManager> ().LoadItem();		
		}
	}



	public void StateObjetActive(bool state, Item selectedItem){
		itemToUse = selectedItem;
		initialise ();
		objetChoiceActive = state;
		gameObject.SetActive(state);
		setObjetStats (selectedItem);
		nActive = button.GetLength(0)-1;
		setButtonState(button[nActive], true); //Activation du bouton cancel par default

	}

	void setObjetStats (Item selectedItem) {
		nomObjet.text = selectedItem.getNom ();

		string symboleH = "+";

		if (selectedItem.getHpBonus() < 0) {
			nbHeart.color = Color.red;
			symboleH = "";
		}

		nbHeart.text = symboleH + selectedItem.getHpBonus ();
	}

	void setButtonState (GameObject button, bool isSelected) {
		if (isSelected)
			button.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			button.GetComponent<Image> ().sprite = baseButtonSprite;
	}

	void initialise(){
		foreach (GameObject b in button) {
			setButtonState(b, false);
		}

		nbHeart.color = Color.white;
	}

	void itemActive (GameObject buttonActive, Item itemToUseArg) {
		pStats = player.GetComponent<PlayerStats> ();

		if (buttonActive.name == "Utiliser")
			pStats.useItem (itemToUseArg);
		if (buttonActive.name == "Supprimer")
			pStats.removeItem (itemToUseArg);
	}
}
