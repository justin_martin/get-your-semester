﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class MenuManager : MonoBehaviour {



	[SerializeField] private Sprite baseButtonSprite;
	[SerializeField] private Sprite selectedButtonSprite;
	[SerializeField] private GameObject ObjetsBox;
	[SerializeField] private GameObject StatsBox;
	[SerializeField] private GameObject SauvegarderBox;
	[SerializeField] private GameObject OptionsBox;
	[SerializeField] private GameObject CommandeBox;
	[SerializeField] private GameObject LeaveBox;

	[SerializeField] private GameObject MenuBox;


	private GameObject selectedButton;
	private int indexSelected;
	[SerializeField] private GameObject[] sousMenu;
	public static bool inventaireActive;

	ButtonListener buttonListener;

	// Use this for initialization
	void Start () {	
		buttonListener = FindObjectOfType<ButtonListener>();
	}


	// Update is called once per frame
	void Update () {

		if (inventaireActive && (Input.GetKeyDown(KeyCode.DownArrow) || buttonListener.GetDownArrowClick())) {
			setButtonState (sousMenu [indexSelected], false);
			if (indexSelected == sousMenu.GetLength(0)-1) indexSelected = 0;
			else indexSelected++;
			setButtonState(sousMenu[indexSelected], true);
		}

		if (inventaireActive && (Input.GetKeyDown(KeyCode.UpArrow) || buttonListener.GetUpArrowClick())) {
			setButtonState (sousMenu [indexSelected], false);
			if (indexSelected == 0) indexSelected = sousMenu.GetLength(0)-1;
			else indexSelected--;
			setButtonState (sousMenu[indexSelected], true);
		}

		if (inventaireActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) {
			if (sousMenu [indexSelected].name == "Stats")
				ShowStats ();
			else if (sousMenu [indexSelected].name == "Sauvegarder")
				ShowSauvegarde ();
			else if (sousMenu [indexSelected].name == "Commandes")
				showCommandes ();
			else if (sousMenu [indexSelected].name == "Leave")
				showLeave ();
		}
		if (inventaireActive && (Input.GetKeyDown (KeyCode.Escape) || buttonListener.GetButtonBClick ())) {
			StateInventaire (false);
		}
	}

	public void StateInventaire(bool state){
		inventaireActive = state;
		gameObject.SetActive(state);
		cleanInventaire ();
		indexSelected = 0;
		setButtonState (sousMenu [indexSelected], true);
	}

	void cleanInventaire () {
		foreach (GameObject g in sousMenu)
			setButtonState (g, false);
	}

	void setButtonState (GameObject buttonSelected, bool state) {
		if (state)
			buttonSelected.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			buttonSelected.GetComponent<Image> ().sprite = baseButtonSprite;
	}


	public void ShowObjets(){
		MenuBox.SetActive (false);
		ObjetsBox.GetComponent<ObjetsManager> ().StateObjets (true);
	}

	public void ShowSauvegarde(){
		MenuBox.SetActive (false);
		SauvegarderBox.GetComponent<SaveManager> ().StateSave (true);
	}

	public void ShowStats(){
		MenuBox.SetActive (false);
		StatsBox.GetComponent<StatsManager> ().StateStats (true);
	}

	public void ShowOptions(){
		MenuBox.SetActive (false);
		OptionsBox.SetActive (true);
	}

	void showCommandes () {
		MenuBox.SetActive (false);
		CommandeBox.GetComponent<CommandeManager> ().StateCommande (true);
	}

	void showLeave () {
		MenuBox.SetActive (false);
		LeaveBox.GetComponent<LeaveManager> ().StateLeave (true);
	}
}
