﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowMenu : MonoBehaviour {

	public GameObject Objets;
	public GameObject Stats;
	public GameObject Capacites;
	public GameObject Sauvegarder;
	public GameObject Options;

	public GameObject MenuBox;
	// Use this for initialization
	void Start () {	
		Objets.SetActive (false);
		Stats.SetActive (false);
		Capacites.SetActive (false);
		Sauvegarder.SetActive (false);
		Options.SetActive (false);
	}

	public void ShowObjets(){
		MenuBox.SetActive (false);
		Objets.SetActive (true);
	}

	public void ShowCapacites(){
		MenuBox.SetActive (false);
		Capacites.SetActive (true);
	}

	public void ShowSauvegarde(){
		MenuBox.SetActive (false);
		Sauvegarder.SetActive (true);
	}

	public void ShowStats(){
		MenuBox.SetActive (false);
		Stats.SetActive (true);
	}

	public void ShowOptions(){
		MenuBox.SetActive (false);
		Options.SetActive (true);
	}

}
