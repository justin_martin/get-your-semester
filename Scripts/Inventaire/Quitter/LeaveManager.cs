﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaveManager : MonoBehaviour {

	private bool leaveActive;
	private int indexSelected;

	[SerializeField] private Sprite baseButtonSprite;
	[SerializeField] private Sprite selectedButtonSprite;

	[SerializeField] private GameObject MenuBox;
	[SerializeField] private GameObject[] sousMenu;

	ButtonListener buttonListener;



	private void cleanSousMenu () {
		foreach (GameObject g in sousMenu)
			setButtonState (g, false);
	}

	public void StateLeave(bool state){
		leaveActive = state;
		gameObject.SetActive(state);
		cleanSousMenu ();
		indexSelected = sousMenu.GetLength(0)-1;
		setButtonState(sousMenu[indexSelected], true); //Activation du bouton cancel par default
	}

	void setButtonState (GameObject buttonSelected, bool state) {
		if (state)
			buttonSelected.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			buttonSelected.GetComponent<Image> ().sprite = baseButtonSprite;
	}

	// Use this for initialization
	void Start () {	
		buttonListener = FindObjectOfType<ButtonListener> ();
	}

	// Update is called once per frame
	void Update () {

		if (leaveActive && ((Input.GetKeyDown(KeyCode.RightArrow) || buttonListener.GetRightArrowClick()))) {
			setButtonState (sousMenu[indexSelected], false);
			if (indexSelected == sousMenu.GetLength (0) - 1) indexSelected = 0;
			else indexSelected++;
			setButtonState (sousMenu[indexSelected], true);
		}

		if (leaveActive && (Input.GetKeyDown(KeyCode.LeftArrow) || buttonListener.GetLeftArrowClick())) {
			setButtonState (sousMenu[indexSelected], false);
			if (indexSelected == 0) indexSelected = sousMenu.GetLength (0) - 1;
			else indexSelected--;
			setButtonState (sousMenu[indexSelected], true);
		}

		if (leaveActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) {
			if (sousMenu [indexSelected].name == "Quitter") {
				gameObject.SetActive (false);
				MenuBox.SetActive (true);
				Initiate.Fade("GameMenu", Color.black, 2.0f);
			}
			else if (sousMenu [indexSelected].name == "Annuler") {
				gameObject.SetActive (false);
				MenuBox.SetActive (true);
			}
		}
	}

}
