﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuHolder : MonoBehaviour {

	[SerializeField] private GameObject menuBox;

    private MenuManager iMan;
    private ButtonListener buttonListener;

    // Use this for initialization
    void Start () {
		iMan = menuBox.GetComponent<MenuManager>();
        buttonListener = FindObjectOfType<ButtonListener>();
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Inventaire") || buttonListener.GetButtonMenuClick()) {
		    iMan.StateInventaire(!MenuManager.inventaireActive);
        }
    }
}
