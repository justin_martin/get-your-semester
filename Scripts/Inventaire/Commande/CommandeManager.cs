using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

class CommandeManager : MonoBehaviour {

	public bool commandeActive;
	public bool useTuto;
	[SerializeField] private GameObject inventaire;

	[SerializeField] private GameObject[] commandePannelTab;
	private Dictionary<string, GameObject> commandePannelSystem;

	private ButtonListener buttonListener;

	void Start(){
		buttonListener = FindObjectOfType<ButtonListener> ();
		commandePannelSystem = new Dictionary<string, GameObject> ();
		commandePannelSystem.Add ("Android", commandePannelTab [0]);
		commandePannelSystem.Add ("PC", commandePannelTab [1]);
	}

	void Update(){
		if (!useTuto && commandeActive && (Input.GetKeyDown (KeyCode.Escape) || buttonListener.GetButtonBClick()))
			StateCommande (false);	
	}

	public void StateCommande(bool state){
		commandeActive = state;
		GameObject pannelSelected;
	
		#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR
			commandePannelSystem.TryGetValue("Android", out pannelSelected);
		#else
			commandePannelSystem.TryGetValue("PC", out pannelSelected);
		#endif
		pannelSelected.SetActive (state);

		if (!state && !useTuto) {
			inventaire.SetActive (true);
			inventaire.GetComponent<MenuManager> ().StateInventaire (true);
		}
		if(!state)	useTuto = false;
	}
}

