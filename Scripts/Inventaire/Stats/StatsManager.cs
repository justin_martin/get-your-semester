﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour {


	public GameObject player;
	// Part 1
	public GameObject playerSprite;
	public Text playerName;
	public Text playerLvl;

	// Part 2
	public Text playerHP;

	// Part 3
	public Sprite gemmeVide;
	public GameObject contentGemme;
	public GameObject ultimeGemme;

	//To see if menu is active
	private bool statsActive;

	[SerializeField] private GameObject menuBox;
	//To laod stats
	private PlayerStats pStats;
	private ButtonListener buttonListener;


	[SerializeField] private Sprite[] gemmesSprites;

	// Use this for initialization
	void Start () {}

	public void StateStats(bool state){
		buttonListener = FindObjectOfType<ButtonListener>();
		statsActive = state;
		gameObject.SetActive(state);
		initGemme ();
		LoadStats();
		LoadGemmes();
	}

	void LoadStats () {
		pStats = player.GetComponent<PlayerStats> ();

		playerSprite.GetComponent<Image> ().sprite = pStats.getSprite (); 
		playerName.text = pStats.pseudo;
		playerLvl.text = "" + pStats.lvl;

		playerHP.text = pStats.health + " / " + pStats.healthMax;
		}

	void LoadGemmes () {

		int count = 0;
		foreach (bool b in pStats.gemmes)
			if(b) count++;

		if (count==pStats.gemmes.Length) {
			holdGemme ();
			displayUltimeGemme ();
		}
		else {
			int i = 0;
			foreach (Transform child in contentGemme.transform) {
				if (!pStats.gemmes [i])
					child.gameObject.GetComponent<Image> ().sprite = gemmeVide;
				i++;
			}
		}
	}

	void holdGemme () {
		foreach (Transform child in contentGemme.transform) {
			child.gameObject.SetActive (false);
		}
	}

	void displayUltimeGemme () {
		ultimeGemme.SetActive (true);
	}

	// Update is called once per frame
	void Update () {
		if ((Input.GetKeyDown (KeyCode.Escape) || buttonListener.GetButtonBClick ()) && statsActive) {
			menuBox.GetComponent<MenuManager> ().StateInventaire (true);
			StateStats (false);
		}
	}

	void initGemme () {
		int i = 0;
		foreach (Transform child in contentGemme.transform) 
			child.gameObject.GetComponent<Image> ().sprite = gemmesSprites [i++];
	}
}
