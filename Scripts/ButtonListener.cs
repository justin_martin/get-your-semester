﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ButtonListener : MonoBehaviour {

    public GameObject control_ui;

    // Pressdown / up
    public bool buttonAPress;
    public bool buttonBPress;
    public bool buttonPausePress;
    public bool buttonMenuPress;

    //Click
    public bool buttonAClick;
    public bool buttonBClick;
    public bool buttonPauseClick;
    public bool buttonMenuClick;
	//public bool buttonInventaireClick;

	//Direction
	//Pressdown / up
	public bool leftArrowPress;
	public bool rightArrowPress;
	public bool upArrowPress;
	public bool downArrowPress;
	//Click
	public bool leftArrowClick;
	public bool rightArrowClick;
	public bool upArrowClick;
	public bool downArrowClick;

	//Time 
	//private DateTime deltaInventaire;

    // Méthodes pour les events triggers
	//ButtonA
	public void ButtonAPressed(){
		buttonAPress = true;
		//buttonAClick = true;
    }

    public void ButtonARelease() {
        buttonAPress = false;
        buttonAClick = false;
    }

	public void ButtonAClick() {
		buttonAClick = true;
	}
	//ButtonB
    public void ButtonBPressed() {
        buttonBPress = true;
    }

    public void ButtonBRelease() {
        buttonBPress = false;
        buttonBClick = false;
    }

	public void ButtonBClick() {
		buttonBClick = true;
	}

	//Button Pause
    public void ButtonPausePressed() {
        buttonPausePress = true;
    }
    public void ButtonPauseRelease() {
        buttonPausePress = false;
        buttonPauseClick = false;
    }

	public void ButtonPauseClick() {
		buttonPauseClick = true;
	}

	//Button Menu
    public void ButtonMenuPressed() {
        buttonMenuPress = true;
    }
    public void ButtonMenuRelease() {
        buttonMenuPress = false;
        buttonMenuClick = false;
    }

	public void ButtonMenuClick() {
		buttonMenuClick = true;
	}
    
	//Left Arrow
	public void LeftArrowPressed() {
		leftArrowPress = true;
	}

	public void LeftArrowRelease() {
		leftArrowPress = false;
		leftArrowClick = false;
	}

	public void LeftArrowClick() {
		leftArrowClick = true;
	} 

	//Right Arrow
	public void RightArrowPressed() {
		rightArrowPress = true;
	}

	public void RightArrowRelease() {
		rightArrowPress = false;
		rightArrowClick = false;
	}

	public void RightArrowClick() {
		rightArrowClick = true;
	} 

	//Up Arrow
	public void UpArrowPressed() {
		upArrowPress = true;
	}

	public void UpArrowRelease() {
		upArrowPress = false;
		upArrowClick = false;
	}

	public void UpArrowClick() {
		upArrowClick = true;
	} 

	//Down Arrow
	public void DownArrowPressed() {
		downArrowPress = true;
	}

	public void DownArrowRelease() {
		downArrowPress = false;
		downArrowClick = false;
	}

	public void DownArrowClick() {
		downArrowClick = true;
	} 

    // Méthode GET pour les scripts (ex: InventaireHolder, DialogueHolder)
    public bool GetButtonAClick() {
        if (buttonAClick) {
            buttonAClick = false;
            return true;
        }
        return false;
    }

    public bool GetButtonBClick() {
        if(buttonBClick) {
            buttonBClick = false;
            return true;
        }
        return false;
    }

	public bool GetButtonPauseClick() {
        if (buttonPauseClick) {
            buttonPauseClick = false;
            return true;
        }
        return false;
    }

	public bool GetButtonMenuClick() {
        if (buttonMenuClick) {
            buttonMenuClick = false;
            return true;
        }
        return false;
    }

	/*public bool GetButtonAPressed(){
		return buttonAPress;
	}

	public bool GetButtonAPressed(){
		return buttonAPress;
	}

	public bool GetButtonAPressed(){
		return buttonAPress;
	}

	public bool GetButtonAPressed(){
		return buttonAPress;
	}*/

	public bool GetLeftArrowClick() {
		if (leftArrowClick) {
			leftArrowClick = false;
			return true;
		}
		return false;
	}

	public bool GetRightArrowClick() {
		if (rightArrowClick) {
			rightArrowClick = false;
			return true;
		}
		return false;
	}

	public bool GetUpArrowClick() {
		if (upArrowClick) {
			upArrowClick = false;
			return true;
		}
		return false;
	}

	public bool GetDownArrowClick() {
		if (downArrowClick) {
			downArrowClick = false;
			return true;
		}
		return false;
	}

    // Selon la présence d'un élement par dessus l'UI (ex: Dialogue) on cache ou non l'UI
    public void DefineUIMobileState(bool isOverlay) {
        if (isOverlay)
            HideUIMobile();
        else
            ShowUIMobile();
    }

    public void SetStateMobileUI(bool state) {
        control_ui.SetActive(state);
		buttonAClick = false;
		buttonAPress = false;
	
		buttonBClick = false;
		buttonBPress = false;

		buttonMenuClick = false;
		buttonMenuPress = false;

		buttonPausePress = false;
		buttonMenuClick = false;
    }

    public void HideUIMobile() {
        SetStateMobileUI(false);
    }

    public void ShowUIMobile() {
		#if (UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR)
			SetStateMobileUI(true);
		#endif
    }

	void Update() {	}

}
