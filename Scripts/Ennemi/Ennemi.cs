﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Ennemi : MonoBehaviour {

	public string pseudo;

	public int health;

	public TextAsset questionsFile; // là-dedans y a le fichier texte contenant les questions et réponses
	public List<Question> questionsList = new List<Question> ();

	public int MaxHealth;
	public int MaxMana;

	public Sprite AvatarFace;
	public Sprite leftFace;
	public Sprite gemme;

	[HideInInspector] public bool isLive;
	private int indexQuestion;

	public TextAsset winFight, loseFight, alreadyWin;

	// Use this for initialization
	void Start () {
		extractQuestions ();
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0)
			isLive = false;
		else
			isLive = true;
	}

	public void sufferDamage (int damage) {
		health -= damage;
	}

	public Question getQuestion(){
		Debug.Log (indexQuestion);	
		Question question = questionsList[indexQuestion];
		indexQuestion++;
		return question;
	}

	public void extractQuestions () {
		indexQuestion = 0;
		string[] questions = questionsFile.text.Replace ("\\n", "").Replace ("/**/", "\n").Split ('%');
		foreach (string question in questions) {
			string[] questionData = question.Split ('_');
		
			Question qu = new Question (
				              questionData [0].Remove (0, 2),
				              questionData [1].Remove (0, 2),
				              questionData [2].Remove (0, 2),
				              questionData [3].Remove (0, 2),
				              questionData [4].Remove (0, 2));
			questionsList.Add (qu);
		}
	}

	public void initQuestion(){
		var random = new System.Random ();
		indexQuestion = 0;
		questionsList.Sort ((x, y) => random.Next(-1, 2));
	}

}