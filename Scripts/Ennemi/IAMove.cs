﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAMove : MonoBehaviour {

	private Rigidbody2D rbody;
	public Animator anim;

	private float speedMove = 1.0f;

	// Use this for initialization
	void Start() {
		rbody = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update() {
		rbody.constraints = RigidbodyConstraints2D.FreezeRotation;
	}

	public void moveTop(float delay)
	{
		canMove (true);
		rbody.MovePosition(rbody.position + new Vector2(0, speedMove * delay));
		anim.SetFloat("input_x", 0);
		anim.SetFloat("input_y", 1);
		canMove (false);
	}

	public void moveBottom(float delay)
	{
		rbody.MovePosition(rbody.position + new Vector2(0, -speedMove * delay));
		anim.SetFloat("input_x", 0);
		anim.SetFloat("input_y", -1);
	}

	public void moveLeft(float delay)
	{
		rbody.MovePosition(rbody.position + new Vector2(-speedMove * delay, 0));
		anim.SetFloat("input_x", -1);
		anim.SetFloat("input_y", 0);
	}

	public void moveRight(float delay)
	{
		rbody.MovePosition(rbody.position + new Vector2(speedMove * delay, 0));
		anim.SetFloat("input_x", 1);
		anim.SetFloat("input_y", 0);
	}

	public void canMove(bool state){
		if (state)
			rbody.bodyType = RigidbodyType2D.Dynamic;
		else rbody.bodyType = RigidbodyType2D.Static; 
	}
}
