﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	[SerializeField] private AudioSource audioSourceEffect;
	[SerializeField] private AudioSource audioSourceSound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void playSoundEffect(AudioClip audioclip){
		audioSourceEffect.PlayOneShot (audioclip);
	}

	public void playSoundGlobal(AudioClip audioclip){
		audioSourceSound.clip = audioclip;
		audioSourceSound.Play ();
	}
}
