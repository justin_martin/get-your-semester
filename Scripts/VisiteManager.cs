﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Created by Justin "*PheniXos" Martin
 * Ce script gère la visite le l'IUT qu'y est effectué par l'ADIIL
 * */

public class VisiteManager : MonoBehaviour {

	public static bool visiteActive;

	//3 Sprites représentant les 3 états du bouton
	[SerializeField] private Sprite baseButtonSprite;
	[SerializeField] private Sprite selectedButtonSprite;
	[SerializeField] private Sprite disabledButtonSprite;

	[SerializeField]
	public GameObject player; 

	private GameObject selectedButton; //bouton selectionné
	private int indexSelected; //index du bouton selectionné
	[SerializeField]
	private GameObject[] sousMenu; //les différents boutons du menu

	[SerializeField]
	private GameObject[] adiil; //membre de l'adiil de la vue
	public GameObject[] adiilBox; //zone de dialogue ratachée au membre de l'adiil dans la section qu'il présente

	private GameObject[] adiilBoxCop; //temps Value ?? 

	[SerializeField]
	private GameObject Fade; 

	[SerializeField]
	private GameObject EndBox; // Box de fin de présentation

	private GameObject adiilBox_temp;
	private ButtonListener buttonListener;
	// Use this for initialization
	void Start () {	

		if (PlayerPrefs.HasKey ("visited") && PlayerPrefs.GetInt("visited") == 1) {
			foreach(GameObject g in adiil){
				g.SetActive (false);
			}

			foreach(GameObject g in adiilBox){
				g.SetActive (false);
			}

			EndBox.SetActive (false);
		}
		adiilBoxCop = adiilBox;
		buttonListener = FindObjectOfType<ButtonListener> (); //On récupère le lecteur d'événement d'interface
	}
	
	// Update is called once per frame
	void Update () {
		
		if (visiteActive && (Input.GetKeyDown(KeyCode.DownArrow) || buttonListener.GetDownArrowClick())) { //Si la touche de direction bas est appuyée (PC ou UI Mobile)
			setButtonState (sousMenu [indexSelected], false); //desactivation du bouton actuel
			if (indexSelected == sousMenu.GetLength(0)-1) indexSelected = 0; //si on est au bouton le plus bas on selectionne le premier
			else indexSelected++; //sinon le suivant
			setButtonState(sousMenu[indexSelected], true); //on active ce bouton
		}

		if (visiteActive && (Input.GetKeyDown(KeyCode.UpArrow) ||  buttonListener.GetUpArrowClick())) { //Si la touche de direction bas est appuyée (PC ou UI Mobile)
			setButtonState (sousMenu [indexSelected], false);
			if (indexSelected == 0) indexSelected = sousMenu.GetLength(0)-1;
			else indexSelected--;
			setButtonState (sousMenu[indexSelected], true);
		}


		if (visiteActive && (Input.GetKeyDown(KeyCode.Return) || buttonListener.GetButtonAClick())) { //Si le bouton A ou la touche entrer est appuyé
			if (sousMenu [indexSelected].name == "RDC") {
				adiilBox_temp = adiilBox [indexSelected];
				visiteRDC ();
			} else if (sousMenu [indexSelected].name == "1er étage") {
				adiilBox_temp = adiilBox [indexSelected];
				visite1Stage ();
			} else if (sousMenu [indexSelected].name == "Bureau ADIIL") {
				adiilBox_temp = adiilBox [indexSelected];
				visiteADIIL ();
			} else if (sousMenu [indexSelected].name == "Skip") {
				StartCoroutine (Fade.GetComponent<Fade> ().DoFadeWithPlayerMove (Constant.Stage1Vector.x, Constant.Stage1Vector.y, true));
				StartCoroutine (endVisite ());
			}

			disableSelectedButton (); //on supprime ensuite le bouton selectinner afin de completement le desactivé
		}
	}

	public void StateVisite(bool state){
		gameObject.SetActive (state); //activation de la box Visite
	
		if (sousMenu.Length == 1) { //Si il n'y a plus aucun bouton (cf tous supprimé)
			StartCoroutine (Fade.GetComponent<Fade> ().DoFadeWithPlayerMove (Constant.Stage1Vector.x, Constant.Stage1Vector.y, true));
			StartCoroutine (endVisite ());
		}
		else {
			visiteActive = state;
			cleanVisite (); //On deselectionne l'ancien bouton selectionner
			indexSelected = 0;
			if (state)
				setButtonState (sousMenu [indexSelected], true);
		}

	}

	public void cleanVisite(){
		foreach (GameObject g in sousMenu) { //On desélectionne tous les bouton
			setButtonState (g, false);
		}
	}

	void setButtonState (GameObject buttonSelected, bool state) { 
		if (state)
			buttonSelected.GetComponent<Image> ().sprite = selectedButtonSprite;
		else
			buttonSelected.GetComponent<Image> ().sprite = baseButtonSprite;
	}


	public void visiteRDC(){
		player.GetComponent<Animator> ().SetFloat ("input_x", 0);
		player.GetComponent<Animator> ().SetFloat ("input_y", 1);
		StartCoroutine(Fade.GetComponent<Fade>().DoFadeWithPlayerMove(Constant.RDCVisiteVecteur.x, Constant.RDCVisiteVecteur.y, true));
		StartCoroutine(deleteStage());
	}

	public void visite1Stage() {
		player.GetComponent<Animator> ().SetFloat ("input_x", 0);
		player.GetComponent<Animator> ().SetFloat ("input_y", 1);
		StartCoroutine(Fade.GetComponent<Fade>().DoFadeWithPlayerMove(Constant.Stage1VisiteVecteur.x, Constant.Stage1VisiteVecteur.y,true));
		StartCoroutine(deleteStage());
	}

	public void visiteADIIL() {
		player.GetComponent<Animator> ().SetFloat ("input_x", 0);
		player.GetComponent<Animator> ().SetFloat ("input_y", 1);
		StartCoroutine(Fade.GetComponent<Fade>().DoFadeWithPlayerMove(Constant.ADIILVisiteVecteur.x, Constant.ADIILVisiteVecteur.y,true));
		StartCoroutine(deleteStage());
	}

	void disableSelectedButton () { 
		sousMenu [indexSelected].GetComponent<Image> ().sprite = disabledButtonSprite;
		if(sousMenu.GetLength (0) == 1) sousMenu = null;
		else {
			GameObject[] sousMenuTemp = new GameObject[sousMenu.GetLength (0)-1];
			int cpt = 0;
			int cpt_temp = 0;
			foreach (GameObject s in sousMenu) {
				if (cpt != indexSelected) {
					sousMenuTemp [cpt_temp] = sousMenu [cpt];
					cpt_temp++;
				}
				cpt++;
			}
			sousMenu = sousMenuTemp;
		}	
	}

	void deleteBox () {
		if (adiilBox.GetLength (0) == 1) adiilBox = null;
		else {
			GameObject[] adiilBoxTemp = new GameObject[adiilBox.GetLength (0)-1];
			int cpt = 0;
			int cpt_temp = 0;
			foreach (GameObject s in adiilBox) {
				if (cpt != indexSelected) {
					adiilBoxTemp [cpt_temp] = adiilBox [cpt];
					cpt_temp++;
				}
				cpt++;
			}
			adiilBox = adiilBoxTemp;
		}
	}

	private IEnumerator deleteStage() {
		
		while (Fade.GetComponent<Fade> ().fadeActive)
			yield return new WaitForSeconds (0.1f);
		
		Destroy (adiil [indexSelected]);

		adiilBox [indexSelected].transform.parent.gameObject.SetActive(true);
		adiilBox [indexSelected].GetComponent<DialogueHolder> ().activeDialog ();		
		deleteBox ();

		gameObject.SetActive(false);
		visiteActive = false;
	}


	private IEnumerator endVisite(){
		PlayerPrefs.SetInt ("visited", 1);
	
		foreach (GameObject g in adiilBoxCop)
			Destroy (g.transform.parent.gameObject);
		
		while (Fade.GetComponent<Fade> ().fadeActive)
			yield return new WaitForSeconds (0.1f);
	
		EndBox.GetComponent<DialogueHolder> ().activeDialog ();
		gameObject.SetActive(false);
		visiteActive = false;	
	}
}
 	