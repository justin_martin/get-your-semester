﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant {

	public static Vector2 introVecteur { get { return new Vector2 (22.3f, -63.59f); } } 
	public static Vector2 introCamerasVecteur { get { return new Vector2 (22.3f, -64.69f); } } 
	public static Vector2 examVecteur { get { return new Vector2 (20.99f, -72.39f); } }
	public static Vector2 endVecteur { get { return new Vector2 (21.42f, -64.84f); } }
	public static Vector2 RDCVisiteVecteur { get { return new Vector2 (21.5f, -64.89f); } }
	public static Vector2 Stage1VisiteVecteur { get { return new Vector2 (73.08f, -38.74f); } }
	public static Vector2 ADIILVisiteVecteur { get { return new Vector2 (82.23f, -60.1f); } }
	public static Vector2 Stage1Vector { get { return new Vector2 (22.41f, -65.58f); } } 
}
