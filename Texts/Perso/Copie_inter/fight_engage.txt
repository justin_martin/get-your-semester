﻿Chargée de communication pour l'IUT de Laval, je suis Cécile Copie, une gardienne du module Conception de documents et d'interfaces numériques.
Mme Copie est très occupée puisqu'elle travaille même pendant vos vacances, et pourtant, elle vous donne cours.
Elle espère vraiment que vous ne lui avez pas fait perdre son temps.
À vous de lui prouver.