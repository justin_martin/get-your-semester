﻿  Que permet l'option ON DELETE ou ON UPDATE lors de la définition de la clé étrangère ?_
Je t'en pose des questions :D ?_
D'effacer immédiatement les données d'une table_
De mettre à jour immédiatement les données d'une table_
De délier les enregistrements entre deux tables par suppression ou mise à jour des données
%
Un dictionnaire des données permet_
D'inventorier les données nécessaires et suffisantes du système d'information_
D'inventorier toutes les données du système d'information même celles qui sont redondantes_
De traduire les données du système d'information en plusieurs langues_
D'indiquer si chaque donnée est un nom, un verbe ou un adjectif
%
Une dépendance fonctionnelle permet d'indiquer_
Que pour un numéro de sécurité sociale j'ai plusieurs noms et prénoms_
Que pour un nom et un prénom, j'ai un numéro de sécurité sociale_
Que pour un nom et un prénom, j'ai plusieurs numéros de sécurité sociale_
Que pour un numéro de sécurité sociale j'ai un seul nom et prénom
%
Quel est l'autre nom d'un MCD ?_
Le schéma "entités-associations"_
Le modèle logique des données_
Le modèle de langage unifié_
Le schéma "entités-relations"
%
Que signifie le terme SQL ?_
Structured Query Language_
Structured Query Literature_
Super Query Language_
Structured Qwerty Language
%
Une clé étrangère permet_
De lier des enregistrements de deux tables différentes dans la base_
D'ouvrir n'importe quelle porte_
D'accéder à un site web étranger inaccessible en France_
D'importer plus facilement de nouvelles données dans la base 
