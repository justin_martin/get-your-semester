﻿Qu'est ce qu'un Algorithme ?_
Une suite d'actions ordonnées_
Un logiciel de calcules scientifique_
Un ancien système d'exploitation_
Un programme compris par la machine
%
L’affectation d’une valeur à une variable consiste à :_
Associer une valeur à une variable_
Comparer la valeur d’une variable à une autre valeur_
Incrémenter une variable_
Déplacer une variable en mémoire_
%
Dans la boucle “ TANT QUE ”, la condition est vérifiée :_
Avant chaque exécution du traitement_
Après chaque exécution du traitement_
Pendant l’exécution du traitement_
Pendant et après chaque exécution_ du traitement
%
A quoi sert un langage de programmation?_
À traduire un algorithme de manière compréhensible par un ordinateur._
À écrire un document texte._
À produire des organigrammes corrects._
À envoyer les signaux d'entrée-sortie aux périphériques de l'ordinateur.
%
On ne sort jamais d’une boucle si la condition d’arrêt_
est toujours vraie_
ne varie pas en cours d’exécution._
ne contient pas d’opérateurs booléens._
est toujours fausse.
%
Le résultat d’une comparaison est une valeur_
Booléenne_
Réelle_
Qui dépend du type des arguments_
Entière

