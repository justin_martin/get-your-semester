﻿  Soit E = {a,b,c,d} et F = {a,c}. A-t-on :_
F est inclus dans E_
a est inclus dans E_
{a} appartient à E_
E est inclus dans F
%
Quel est le cardinal de l'ensemble A∪B ?_
|A| + |B| - |A∩B|_
|A| + |B| + |A∩B|_
|B| - |A∩B|_
|A| - |A∩B|
%
Laquelle des propositions suivantes n'est pas une proposition logique ?_
"Cette phrase est fausse."_
"2 est pair"_
"Si la terre est plate alors 2 est pair"_
"3 est pair et la somme des angles d'un triangle est égale à 180*"
%
Quelle est la négation de la phrase  "Si la terre est plate alors 2 est pair" ?_
La terre est plate et 2 est impair._
Si la terre n'est pas plate alors 2 n'est pas pair._
Si la terre n'est pas plate alors 2 est pair._
La terre est plate ou 2 est pair.
%
La relation ≤ sur l'ensemble des entiers naturels est : _
antisymétrique_
symétrique_
non réflexive_
non transitive
%
Laquelle des situations suivantes est impossible pour une relation R ?_
R est non réflexive, symétrique, antisymétrique et non transitive_
R est réflexive, symétrique, antisymétrique et transitive_
R est non réflexive, non symétrique, non antisymétrique et non transitive_
R est réflexive, non symétrique, antisymétrique et transitive