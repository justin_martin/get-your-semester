﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMenuManager : MonoBehaviour {

	[SerializeField] private AudioClip menuSound;
	[SerializeField] private AudioClip effectClickButton;

	void Start(){
		SoundManager soundMan = FindObjectOfType < SoundManager >();
		soundMan.playSoundGlobal (menuSound);
	}
	

	public void playSoundClickEffect(){
		SoundManager soundMan = FindObjectOfType < SoundManager >();
		soundMan.playSoundEffect (effectClickButton);
	}
}
